# coding=utf-8
from ci import *

if __name__ == '__main__':
    zip_name = "{}-{}-{}.zip".format(env("PLATFORM"), env("CI_COMMIT_REF_SLUG"), env("SELECT_MACHINE"))
    zip_py(zip_name, ci_home("cache-lib", env("CI_PROJECT_PATH"), env("PLATFORM") + "-" + env("CI_COMMIT_REF_SLUG"), "UnityProj/Library/com.unity.addressables"))
    backup_url = env("GAME_ID") + "/backup/ab/" + zip_name
    upload_nexus(zip_name, backup_url)
