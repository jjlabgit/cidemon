# coding=utf-8
# pylint: disable=print-statement
import sys
import json

def write_manifest(filename, env_name, key = 'version'):
    if env(env_name):
        data = json.load(open(filename))
        data[key] = env(env_name)
        json.dump(data, open(filename, 'w'))

if __name__ == "__main__":
    write_manifest("UnityProj/Assets/Resources/version.manifest", "RESOURCE_VERSION")
    write_manifest("UnityProj/Assets/Resources/dolphin_program_version.manifest", "DOLPHIN_PROGRAM_VERSION")
    write_manifest("UnityProj/Assets/Resources/dolphin_source_version.manifest", "DOLPHIN_SOURCE_VERSION")
