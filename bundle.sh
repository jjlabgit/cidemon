#!/bin/sh
set -e

if [ "true" == "$CLEAN_LIBRARY" ]; then
    rm -rf UnityProj/Library/*
    CLEAN_CACHE=true
fi
if [ "true" == "$CLEAN_CACHE" ]; then
    rm -rf UnityProj/AssetBundles/
    BUNDLE_FORCE_REBUILD=true
fi
# 等待编译完成(这句话看起来像个废话，其实它是有作用的. bug: 例如：正在编译，稍后再试,就不build bundle了)
# 加上这个令u3d启动一下，load一下盘，尽量避免下面的调用打ab出现正在编译中
${UNITY_EXE_PATH} -batchmode -projectPath ${CI_PROJECT_DIR}/UnityProj -quit -logFile /dev/stdout
# 用一种比较无奈的方法暂时解决“正在编译中……”的错误。

# 执行 项目内写的构建ab的方法
for (( i=0; i<3; i++ )); do
    RESULT=0
    ${UNITY_EXE_PATH} \
        -batchmode \
        -projectPath ${CI_PROJECT_DIR}/UnityProj \
        -buildTarget ${BUILD_TARGET} \
        -executeMethod ${BUILD_BUNDLE_METHOD} \
        -isForceRebuild ${BUNDLE_FORCE_REBUILD} \
        -platformIndex ${PLATFORM_INDEX} \
        -enableEncodeLua ${BUNDLE_ENABLE_ENCODE_LUA} \
        -language ${BUILD_LANG_CONF} \
        -gameConfigBranchName ${GAMECONFIG_BRANCH_NAME} \
		-branchName ${CI_COMMIT_REF_SLUG} \
        -quit \
        -logFile | tee ./log.txt || RESULT=${PIPESTATUS[0]}
    echo "The return code of Unity is $RESULT"
    KEY_LOG=$(cat ./log.txt | grep "Failed executing selected SpritePackerPolicy.ArgumentException") || true
    if [[ "" != "$KEY_LOG" ]]; then
        echo $'\x1b[31;1m构建出错:Failed executing selected SpritePackerPolicy.ArgumentException\x1b[0;m'
        exit 2
    fi
    KEY_LOG=$(cat ./log.txt | grep "Receiving unhandled NULL exception") || true
    if [[ "" != "$KEY_LOG" ]]; then
        echo $'\x1b[31;1m构建出错:Receiving unhandled NULL exception\x1b[0;m'
        exit 3
    fi
    KEY_LOG=$(cat ./log.txt | grep "Aborting batchmode due to failure") || true
    if [[ "" != "$KEY_LOG" ]]; then
        echo $'\x1b[31;1m构建出错:Aborting batchmode due to failure\x1b[0;m'
        exit 5
    fi
    KEY_LOG=$(cat ./log.txt | grep "EditorApplicationIsCompiling10086") || true
    # Unity返回非零值，且不是“IsCompiling”状态时，要中断编译
    if [[ "" != "$KEY_LOG" ]]; then
        # 暂停一小段时间，以确保旧的Unity完全退出
        sleep 5
        continue
    elif [[ "" == "$KEY_LOG" && "$RESULT" -ne 0 ]]; then
        echo $'\x1b[31;1m构建出错，详情请查询日志\x1b[0;m'
        exit $RESULT
    else
        break
    fi
done

KEY_LOG=$(cat ./log.txt | grep "EditorApplicationIsCompiling10086") || true
# 尝试编译三次之后依然失败，会直接中断构建
if [[ "" != "$KEY_LOG" ]];then
    echo $'\x1b[31;1m构建出错：EditorApplicationIsCompiling10086，建议找CI管理员复制其他分支缓存\x1b[0;m'
    exit 4
fi
