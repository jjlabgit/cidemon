$ErrorActionPreference = "Stop";

$lastLuaVersionPath="C:/gitlab-runner/${Last_Lua_Sha_Folder}/${CI_PROJECT_PATH}/${CI_COMMIT_REF_SLUG}/${Last_Lua_Sha_File_Name}"
if (Test-Path ${lastLuaVersionPath}) {
    $shaVersion=Get-Content -Path $lastLuaVersionPath
}else{
    echo "构建出错，找不到${lastLuaVersionPath}";
    exit 1;
}

echo "lastBuildLuaSha = ${shaVersion}"

$process = Start-Process "${UNITY_EXE_PATH}" -ArgumentList `
    "-batchmode `
    -projectPath ${CI_PROJECT_DIR}/UnityProj `
    -executeMethod ${BUILD_LUA_BUNDLE_METHOD} `
    -shaVersion ${shaVersion} `
    -resVersion ${RESOURCE_VERSION} `
    -branchName ${CI_COMMIT_REF_SLUG} `
    -quit `
    -nographics `
    -logFile log.txt" `
    -PassThru;

Start-Sleep 10;
$job = Start-Job -Scriptblock {param($p) Get-Content -Path $p/log.txt -Wait | ForEach-Object {$_;}} -ArgumentList (pwd).Path;
while ($process.HasExited -eq $false) {
    Receive-Job $job;
}
Receive-Job $job;
Stop-Job $job;
echo "Unity return code is :" $process.ExitCode;

if ($process.ExitCode -ne 0){
    echo "构建出错，详情请查询日志";
    exit $process.ExitCode;
}

cp -R -Force UnityProj/AssetBundles/${PLATFORM}/luascripts ./luascripts;
python CITools/generate-filemanifest.py;
if ($LASTEXITCODE -ne 0) {exit 1;}

Get-ChildItem ./luascripts -recurse -include *.manifest | Remove-Item;

# 修改上一次lua打包版本号为最新，先保存到temp文件夹
$newShaVersion = (git rev-parse HEAD)
echo "shaVersion update to ${newShaVersion}"
$lastLuaVersionPath="C:/gitlab-runner/${Last_Lua_Sha_Folder}/${CI_PROJECT_PATH}/${CI_COMMIT_REF_SLUG}"
mkdir $lastLuaVersionPath -force
Set-Content -Path ${lastLuaVersionPath}/${Last_Lua_Sha_Temp_File_Name} -Value ${newShaVersion}