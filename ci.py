# coding=utf-8

import os
import sys
import shutil
import re
import subprocess
import platform
import zipfile
import ftplib
import json
import time

# 需要额外pip install
from colorama import Fore,Back,Style
import requests

def ci_home(*paths):
    if env("CI_HOME_DIR"):
        ci_home_dir = env("CI_HOME_DIR")
    elif platform.system() == "Windows":
        ci_home_dir = "C:/gitlab-runner"
    else:
        ci_home_dir = os.path.expanduser("~")
    for path in paths:
        if path[0] == "/" or path[0] == "\\":
            path = path[1:]
        ci_home_dir = os.path.join(ci_home_dir, path)
    return ci_home_dir

def __print_colored(text, color):
    choice = {
        'green': Fore.GREEN,
        'cyan': Fore.CYAN,
        'red': Fore.RED,
        'white': Fore.WHITE,
        'yellow': Fore.YELLOW,
        'magenta': Fore.MAGENTA
    }
    if color in choice:
        output_color = choice[color]
    else:
        output_color = Fore.WHITE
    if sys.version_info >= (3, 0):
        sys.stdout.buffer.write((output_color + text + "\n").encode("utf8"))
    else:
        sys.stdout.write(output_color + text + "\n")
    sys.stdout.write(Style.RESET_ALL)
    sys.stdout.flush()

def log(text, color = "green"):
    __print_colored(text, color)

def error(text, code = 1):
    __print_colored(text, "red")
    exit(1)

def __string_to_list(string):
    if isinstance(string, str):
        return [string]
    else:
        return string

def __glob_end(path):
    if "*" not in path[:-2] and (path[-2:] == "/*" or path[-2:] == "\\*"):
        return True
    elif "*" not in path:
        return False
    else:
        error("不支持除以/*结尾之外的其他形式通配符，请考虑搭配find使用")

def __set_from_kwargs(kwargs, key, default_value = None):
    if key in kwargs:
        return kwargs[key]
    else:
        return default_value

def env(key, default_value = None):
    if key in os.environ:
        return os.environ[key]
    else:
        return default_value

def set_env(key, value):
    os.environ[key] = value

def ls(path, kind = "relative", show_hidden = False):
    if "*" in path:
        error("ls不支持通配符")
    result = []
    for element in os.listdir(path):
        if not show_hidden and element[0] == ".":
            continue
        if kind == "absolute":
            element = os.path.join(os.path.abspath(path),element)
        result.append(element)
    return result

def cd(path):
    return os.chdir(path)

def pwd():
    return os.getcwd()

def rm(*paths, **kwargs):
    ignore_errors = __set_from_kwargs(kwargs, "ignore_errors", True)
    for path in paths:
        if __glob_end(path):
            if os.path.isdir(path[:-2]):
                for element in ls(path[:-2], kind = "absolute", show_hidden = True):
                    rm(element, ignore_errors = ignore_errors)
            elif not ignore_errors:
                error(path[:-2] + "不存在！")
        elif not os.path.exists(path):
            if not ignore_errors:
                error(path + "不存在！")
        elif os.path.isdir(path) and not os.path.islink(path):
            shutil.rmtree(path, ignore_errors = ignore_errors)
        elif os.path.exists(path):
            os.remove(path)

def find(path, kind = "relative", type_filter = "directory,file"):
    result = []
    for root, dirs, files in os.walk(path):
        if kind == "absolute":
            parent_path = os.path.abspath(root)
        else:
            parent_path = root
        if "dir" in type_filter:
            for directory in dirs:
                result.append(os.path.join(parent_path, directory).replace("\\","/"))
        if "file" in type_filter:
            for file in files:
                result.append(os.path.join(parent_path, file).replace("\\","/"))
    return result

def grep(string_list, keyword, use_regex = False, mode = "normal", flags = 0 ,ignore_case = True):
    def __grep_string(keyword, string, use_regex, flags):
        if use_regex:
            return re.search(keyword, string, flags)
        elif ignore_case:
            return keyword.lower() in string.lower()
        else:
            return keyword in string
    result = []
    invert_result = []
    if ignore_case:
        flags = flags | re.I
    for element in string_list:
        if __grep_string(keyword, element, use_regex, flags):
            result.append(element)
        else:
            invert_result.append(element)
    if mode == "invert":
        return invert_result
    else:
        return result

def shell(platform_command, allow_failure = False, stdout = None, timing = True, blocking = True):
    time_start = time.time()
    if isinstance(platform_command, str):
        platform_command = {"Windows":platform_command,"Darwin":platform_command}
    elif not isinstance(platform_command, dict):
        error('参数错误：' + platform_command + '。参数需要是一个dict，如{"Windows":"echo $env:PATH","Darwin":"echo $PATH"}')
    for plat in platform_command:
        cur_platform = platform.system().lower()
        cmd = platform_command[plat]
        if plat.lower() == cur_platform or (cur_platform == "darwin" and plat.lower() == "mac"):
            if timing:
                log("正在执行：" + cmd + " ...")
            if plat.lower() == "windows" and stdout:
                process = subprocess.Popen("powershell.exe -Command \" " + cmd + " | Out-File -FilePath " + stdout + "\"")
            elif plat.lower() == "windows":
                process = subprocess.Popen("powershell.exe -Command \" " + cmd + " \"")
            elif stdout:
                process = subprocess.Popen(cmd + " > " + stdout, shell = True)
            else:
                process = subprocess.Popen(cmd, shell = True)
            if blocking:
                process.wait()
                if timing:
                    log("命令" + cmd + "用时： " + str(round(time.time() - time_start, 2)) + "秒")
                if not process.returncode == 0 and not allow_failure:
                    exit(process.returncode)
                else:
                    return process.returncode
            else:
                return process
    return 0

def mkdir(path, mode = 0o777):
    if not os.path.exists(path) and path != "":
        return os.makedirs(path, mode)

def __check_source_and_mkdir_dest(source, dest, auto_mkdir = True, allow_failure = False):
    if not ((__glob_end(source) and os.path.exists(source[:-2])) or os.path.exists(source)):
        if allow_failure:
            return
        error("源文件" + source + "不存在！")
    if ('/' in dest or '\\' in dest) and not os.path.exists(os.path.dirname(dest)) and auto_mkdir:
        mkdir(os.path.dirname(dest))
    if __glob_end(source) and not os.path.isdir(dest):
        mkdir(dest)

def cp(source, dest, auto_mkdir = True, allow_failure = False):
    __check_source_and_mkdir_dest(source, dest, auto_mkdir, allow_failure)
    while source[-1] == "/":
        source = source[:-1]
    shell({
        "Windows": "Copy-Item -R -Force " + source + " " + dest,
        "Darwin": "cp -Rf " + source + " " + dest,
    }, allow_failure = allow_failure, timing = False)

def mv(source, dest, auto_mkdir = True, allow_failure = False):
    __check_source_and_mkdir_dest(source, dest, auto_mkdir, allow_failure)
    while source[-1] == "/":
        source = source[:-1]
    shell({
        "Windows": "Move-Item -Force " + source + " " + dest,
        "Darwin": "mv -f " + source + " " + dest,
    }, allow_failure = allow_failure, timing = False)

def zip_py(zip_name, *zip_list, **kwargs):
    def __zip_py(element):
        if __glob_end(element):
            for f in ls(element[:-2], kind = "absolute", show_hidden = True):
                __zip_py(f)
        else:
            if not os.path.exists(element) and not ignore_not_exist:
                error(element + "不存在！")
            if os.path.isfile(element):
                path_in_zip = os.path.join(parent_dir,os.path.basename(element))
                target.write(element, path_in_zip)
            else:
                start = os.path.dirname(element)
                for root, dirs, files in os.walk(element):
                    for file in files:
                        relpath = os.path.relpath(os.path.join(root, file), start)
                        path_in_zip = os.path.join(parent_dir, relpath)
                        target.write(os.path.join(root, file), path_in_zip)

    parent_dir = __set_from_kwargs(kwargs, "parent_dir", ".")
    mode = __set_from_kwargs(kwargs, "mode", 'a')
    ignore_not_exist = __set_from_kwargs(kwargs, "ignore_not_exist", True)
    wait_list = []
    with zipfile.ZipFile(zip_name, mode, zipfile.ZIP_DEFLATED, allowZip64 = True) as target:
        for element in zip_list:
            if isinstance(element, list):
                for f in element:
                    __zip_py(f)
            else:
                __zip_py(element)

def zip_shell(zip_name, *zip_list):
    if platform.system() == "Windows":
        if shell({"Windows": "Get-Command Compress-7Zip | Out-Null"}, allow_failure = True, timing = False) != 0:
            shell({"Windows": "Install-Module 7Zip4PowerShell -Force -Verbose"}, timing = False)
        for i in range(len(zip_list)):
            command = "Compress-7Zip " + zip_list[i] + " -ArchiveFileName " + zip_name + " -Format Zip "
            if i > 0:
                command = command + "-Append"
            shell({"Windows": command}, timing = False)
    else:
        zip_paths = " "
        for element in zip_list:
            zip_paths = element + " "
        shell({"Darwin": "zip -qr " + zip_name + zip_paths}, timing = False)

def unzip(zip_name, dest = "."):
    with zipfile.ZipFile(zip_name) as target:
        target.extractall(dest)

def curl(url, verify = False, return_str = True, headers = {}, method = "get", data = ""):
    requests.packages.urllib3.disable_warnings()
    if method.lower() == "get":
        result = requests.get(url = url, verify = verify, headers = headers).content
    elif method.lower() == "post":
        result = requests.post(url = url, verify = verify, headers = headers, data = data).content
    elif method.lower() == "put":
        result = requests.put(url = url, verify = verify, headers = headers, data = data).content
    else:
        error("不支持所填写的方法")
    if return_str and sys.version_info >= (3, 0):
        return str(result, encoding = "utf-8")
    elif return_str:
        return str(result)
    else:
        return result

def download(url, filename = None, parent_dir = ".", silent = False, verify = False):
    if not filename:
        filename = os.path.basename(url)
    if not silent:
        log("正在下载：" + url + " ...")
    data = curl(url = url, verify = verify, return_str = False)
    target = os.path.join(parent_dir, filename)
    with open(target, "wb") as f:
        f.write(data)
    if not silent:
        log(target + "下载完成")

def cat(filename, stdout = True, mode = "string"):
    with open(filename, "rb") as f:
        result = f.read().decode("utf-8")
        if stdout:
            log(result, "white")
        if mode == "list":
            return result.splitlines()
        else:
            return result

def write_file(filename, content, mode = "a", newline = True):
    mkdir(os.path.dirname(filename))
    with open(filename, mode) as f:
        f.write(content)
        if newline:
            f.write("\n")

def touch(filename):
    write_file(filename, "", mode = "a", newline = False)

def sed(filename, old_string, new_string, outfile = None):
    content = cat(filename, stdout = False)
    new_content = content.replace(old_string, new_string)
    if not outfile:
        outfile = filename
    write_file(outfile, new_content, mode = "w", newline = False)

def PlistBuddy(filename, *commands, **kwargs):
    if not platform.system() == "Darwin":
        error("只有Mac上可以使用PlistBuddy工具")
    allow_failure = __set_from_kwargs(kwargs, "allow_failure", False)
    stdout = __set_from_kwargs(kwargs, "stdout", None)
    for command in commands:
        shell("/usr/libexec/PlistBuddy -c \"" + str(command) + "\" " + str(filename), allow_failure = allow_failure, stdout = stdout, timing = False)

def upload_nexus(filename, path, domain = "http://repo.bt", user = None, password = None):
    url = domain + "/repository/" + path
    log("正在上传：" + url + " ...")
    if not user:
        user = env("REPO_USER")
    if not password:
        password = env("REPO_PASSWORD")
    if not user or not password:
        error("未在私密变量中配置repo.bt的用户名REPO_USER和密码REPO_PASSWORD")
    response = requests.put(url, data = open(filename, 'rb'), auth = (user, password))
    if int(response.status_code) >= 400:
        log(response.reason, "red")
        error("上传到 " + url + " 失败！返回值：" + str(response.status_code))
    else:
        log("上传 " + url + " 成功！")


def upload_bugly(dir_path, target_platform, bugly_id, bugly_key, version_name, bundleId, log_file = "bugly.log"):
    if not bugly_id or not bugly_key:
        log("警告：BUGLY对应SDK的APP_KEY或APP_ID还没有在私密变量中配置！可能导致无法上传符号表到bugly", "red")
        return
    log("正在上传符号表到bugly...")
    result = shell({
        "Windows": "java -jar {}/buglyqq-upload-symbol.jar -appid {} -appkey {} -bundleid {} -version {} -platform {} -inputSymbol {} | Tee-Object bugly.log".format(env("CI_PROJECT_DIR"), bugly_id, bugly_key, bundleId, version_name, target_platform, dir_path),
        "Darwin": "cp -Rf " + source + " " + dest,
    }, allow_failure = allow_failure)

    if result != 0 or not find_log(log_file, '200 response message: {"statusCode":0,"msg":"success"'):
        error("上传符号表到bugly失败！请联系CI管理员！")

def upload_ftp(filename, path, server = "ftp.bt", port = 21, user = None, password = None, debug_level = 0):
    if not user:
        user = env("FTP_USER")
    if not password:
        password = env("FTP_PASSWORD")
    if not user or not password:
        error("未在私密变量中配置FTP的用户名FTP_USER和密码FTP_PASSWORD")
    ftp = ftplib.FTP()
    ftp.set_debuglevel(debug_level)
    ftp.connect(server, port)
    ftp.login(user, password)
    fp = open(filename, "rb")
    cmd = "STOR " + path
    ftp.storbinary(cmd, fp)
    log("上传到 ftp://" + server + path + " 成功！")

def inform_weixinwork(text, inform_key, msgtype = "text", at = None):
    if not inform_key:
        log("尚未配置企业微信通知...", "yellow")
        return
    url = "https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=" + inform_key
    headers = { 'Content-Type': 'application/json' }
    data = { "msgtype": msgtype }
    data[msgtype] = { "content": text }
    if msgtype == "text" and at:
        data[msgtype]["mentioned_list"] = __string_to_list(at)
    response = requests.post(url, data = json.dumps(data), headers = headers)
    if int(response.status_code) >= 400:
        log("企业微信通知发送失败！", "red")

def unity(**kwargs):
    def __add_arg(args, key, value):
        return args + " -" + key + " " + value
    args = ""
    if "projectPath" not in kwargs:
        args = __add_arg(args, "projectPath", env("CI_PROJECT_DIR") + "/UnityProj")
    log_file = "log.txt"
    is_batchmode = " -batchmode "
    is_quit = " -quit "
    blocking = True
    for arg in kwargs:
        if arg == "blocking":
            blocking = convert_bool(kwargs[arg])
        elif arg == "logFile":
            log_file = kwargs[arg]
        elif arg == "batchmode" and not convert_bool(kwargs[arg]):
            is_batchmode = " "
        elif arg == "quit" and not convert_bool(kwargs[arg]):
            is_quit = " "
        else:
            args = __add_arg(args, arg, str(kwargs[arg]))
    result = shell({
        "Windows": "$process = Start-Process \'" + env("UNITY_EXE_PATH") + "\' -ArgumentList \' "
            # Unity参数
            + is_batchmode
            + args
            + is_quit
            + "-logFile " + log_file
            + " \' -PassThru; "
            # 处理日志
            + "Start-Sleep 10; "
            + "$job = Start-Job -Scriptblock {param($p) Get-Content -Path $p/" + log_file + " -Wait -ReadCount 0 | ForEach-Object {$_;}} -ArgumentList (pwd).Path; "
            + "while ($process.HasExited -eq $false) {Receive-Job $job;} "
            + "Receive-Job $job; "
            + "Stop-Job $job; "
            + "exit $process.ExitCode;",
        "Darwin": env("UNITY_EXE_PATH")
            + is_batchmode
            + args
            + is_quit
            + "-logFile - | tee " + log_file + ";"
            + "exit ${PIPESTATUS[0]};"
    }, allow_failure = True, blocking = blocking)
    shell({"Darwin": "OLD_IFS=$IFS;IFS=$'\n';for each in $(ps -ef | grep 'Unity[.]Licensing[.]Client' | awk '{print $2,$3}'); do if [[ \"${each: -2}\" == ' 1' ]]; then kill ${each%??}; fi; done;IFS=$OLD_IFS;"}, allow_failure = True, timing = False)
    return result

def getUnityPlatformInfo(platform):
    if platform.lower() == "android":
        return "Android", 2
    elif platform.lower() == "ios":
        return "iOS", 3
    else:
        return "Win64", 4

def convert_bool(value, output_type = "bool", mode = "normal"):
    if str(value).lower() in ["true", "t", "yes", "y", "1"]:
        bool_value = True if mode != "invert" else False
    else:
        bool_value = False if mode != "invert" else True
    if output_type == "string":
        return str(bool_value)
    elif output_type == "lower":
        return str(bool_value).lower
    else:
        return bool_value

def find_log(file, keyword):
    return grep(cat(file, stdout = False, mode = "list"), keyword)

def env_bool(key, default_value = False, mode = "normal"):
    return convert_bool(env(key, default_value), mode = mode)

def resolve_shared(smbfs):
    if smbfs:
        info = re.split(r'[:@/,]+', smbfs)
        user = info[0]
        password = info[1]
        ip = info[2]
        path = info[3]
        return user, password, ip, path
    else:
        error("还没有配置WIN64_SERVER环境变量，将取消上传")

def connect_shared(smbfs, mac_mount_path = "win64_server_mount"):
    user, password, ip, path = resolve_shared(smbfs)
    if platform.system() == "Darwin":
        if os.path.isdir(mac_mount_path):
            disconnect_shared(mac_mount_path = mac_mount_path, allow_failure = True)
        mkdir(mac_mount_path)
        returncode = shell("mount -t smbfs //{}:{}@{}/{} {}".format(user, password, ip, path, mac_mount_path), timing = False, allow_failure = True)
        while returncode == 64:
            log("\\\\{}\\{}被占用中，等待5秒后重试...".format(ip, path))
            time.sleep(5)
            returncode = shell("mount -t smbfs //{}:{}@{}/{} {}".format(user, password, ip, path, mac_mount_path), timing = False, allow_failure = True)
        if returncode != 64 and returncode != 0:
            error("连接\\\\{}\\{}共享文件夹出错！".format(ip, path))
        return mac_mount_path
    else:
        remote_path = "\\\\{}\\{}".format(ip, path)
        shell("net use {} /user:RD\\{} {}".format(remote_path, user, password), timing = False)
        return remote_path

def disconnect_shared(mac_mount_path = "win64_server_mount", allow_failure = False):
    shell({"Darwin": "diskutil unmount force " + mac_mount_path}, timing = False, allow_failure = allow_failure)
    rm(mac_mount_path)

def check_job_newest(host = "https://gitlab.bt", newer_job_status = ["created", "pending", "running", "success"], job_name = None):
    if not job_name:
        job_name = env("CI_JOB_NAME")
    unrun_jobs_str = curl("{}/api/v4/projects/{}/jobs".format(host, env("CI_PROJECT_ID")), headers = {"PRIVATE-TOKEN": env("ACCESS_TOKEN")})
    unrun_jobs = json.loads(unrun_jobs_str)
    newest_flag = True
    for job in unrun_jobs:
        if job["name"] == job_name and int(job["id"]) > int(env("CI_JOB_ID")) and job["status"] in newer_job_status:
            newest_flag = False
            break
    return newest_flag

def connect_http_server(smbfs):
    loop_flag = True
    while loop_flag:
        mount_path = connect_shared(smbfs)
        proj_path = "{}/{}/{}".format(mount_path, env("CI_PROJECT_PATH"), env("CI_COMMIT_REF_SLUG"))
        mkdir(proj_path)
        server_lock = "{}/{}".format(proj_path, "httpserver.locked")
        if os.path.isfile(server_lock):
            log("服务器上存在httpserver.locked文件，等待10秒后重试...")
            disconnect_shared()
            time.sleep(10)
        else:
            ci_lock = "{}/{}".format(proj_path, "ci.locked")
            write_file(ci_lock, str(int(time.time())), mode = "w", newline = False)
            loop_flag = False
    return mount_path

def disconnect_http_server(smbfs = None):
    if platform.system() == "Darwin":
        mount_path = "win64_server_mount"
    else:
        if not smbfs:
            smbfs = env("WIN64_SERVER")
        user, password, ip, path = resolve_shared(smbfs)
        mount_path = "\\\\{}\\{}".format(ip, path)
    proj_path = "{}/{}/{}".format(mount_path, env("CI_PROJECT_PATH"), env("CI_COMMIT_REF_SLUG"))
    ci_lock = "{}/{}".format(proj_path, "ci.locked")
    rm(ci_lock)
    disconnect_shared()

def rm_exclude(path, exclude = []):
    if "*" in path:
        error("路径中不能含有通配符！")
    for element in ls(path):
        exclude_flag = False
        for exclude_element in exclude:
            if element == exclude_element:
                exclude_flag = True
                break
        if not exclude_flag:
            rm(os.path.join(path, element))

def cp_exclude(src, dest, exclude = []):
    if "*" in src or "*" in dest:
        error("路径中不能含有通配符！")
    mkdir(dest)
    for element in ls(src):
        exclude_flag = False
        for exclude_element in exclude:
            if element == exclude_element:
                exclude_flag = True
                break
        if not exclude_flag:
            cp(os.path.join(src, element), os.path.join(dest, element))

def gitlab_api(path, api_url = "https://gitlab.bt/api/v4/", method = "get", headers = {}, data = "", warning = True, return_str = True):
    headers["PRIVATE-TOKEN"] = env("ACCESS_TOKEN")
    result = curl(api_url + path, method = method, headers = headers, data = data, return_str = return_str)
    try:
        result_list = json.loads(result)
    except:
        if warning:
            log("Gitlab返回不是Json格式的字符串！返回的字符串为：" + result, "yellow")
        return result
    return result_list
