$ErrorActionPreference = "Stop";

$lastLuaVersionPath="C:/gitlab-runner/${Last_Lua_Sha_Folder}/${CI_PROJECT_PATH}/${CI_COMMIT_REF_SLUG}"
mkdir $lastLuaVersionPath -force
$newShaVersion = (git rev-parse HEAD)
Set-Content -Path ${lastLuaVersionPath}/${Last_Lua_Sha_Temp_File_Name} -Value ${newShaVersion}
echo "update lastBuildLuaSha temp to ${newShaVersion}"