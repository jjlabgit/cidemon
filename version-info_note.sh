#!/bin/sh
set -e
#把当前打包参数的内容 写入到version_info.txt 
mkdir -p UnityProj/Assets/StreamingAssets
echo "Pipeline ID: ${CI_PIPELINE_ID}" > UnityProj/Assets/StreamingAssets/version_info.txt
if [[ "${VERSION_NAME}" != "" ]]; then echo "Version Name: ${VERSION_NAME}" >> UnityProj/Assets/StreamingAssets/version_info.txt; fi
if [[ "${VERSION_CODE}" != "" ]]; then echo "Version Code: ${VERSION_CODE}" >> UnityProj/Assets/StreamingAssets/version_info.txt; fi
if [[ "${RESOURCE_VERSION}" != "" ]]; then echo "Resource Version: ${RESOURCE_VERSION}" >> UnityProj/Assets/StreamingAssets/version_info.txt; fi
if [[ "${DOLPHIN_PROGRAM_VERSION}" != "" ]]; then echo "Dolphin Program Version: ${DOLPHIN_PROGRAM_VERSION}" >> UnityProj/Assets/StreamingAssets/version_info.txt; fi
if [[ "${DOLPHIN_SOURCE_VERSION}" != "" ]]; then echo "Dolphin Source Version: ${DOLPHIN_SOURCE_VERSION}" >> UnityProj/Assets/StreamingAssets/version_info.txt; fi
