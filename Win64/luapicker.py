# coding=utf-8
import os
import sys
from xml.dom.minidom import parse as xmlParse
import json
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from ci import *

def FindPackagePath(package_name):
    library_path = "UnityProj/Library/PackageCache"
    sub_dirs = os.listdir(library_path)
    for i in range(len(sub_dirs)):
        if package_name in sub_dirs[i]:
            return os.path.join(library_path, sub_dirs[i])

    package_path = "UnityProj/Packages"
    sub_dirs = os.listdir(package_path)
    for i in range(len(sub_dirs)):
        dir_path = os.path.join(package_path, sub_dirs[i])
        if os.path.isdir(dir_path):
            package_json_path = '{}/package.json'.format(dir_path)
            if not os.path.exists(package_json_path):
                continue
            with open(package_json_path, mode='r') as fi:
                package_content = fi.read()
                fi.flush()
                fi.close()

            package_data = json.loads(package_content)
            if package_data['name'] == package_name:
                return os.path.join(package_path, sub_dirs[i])

def Get_Lua_Files(lua_path):
    all_files = []
    target_path = []
    if lua_path.startswith('Packages'):
        # package
        package_name = lua_path.replace('Packages/', '')
        name_last_index = package_name.find('/')
        package_path = FindPackagePath(package_name[0: name_last_index])
        right_path = package_path+package_name[name_last_index:len(package_name)]
        for root, subdirs, files in os.walk(right_path):
            for file_path in files:
                if file_path.endswith('.lua'):
                    relative_path = os.path.join(root, file_path)
                    relative_path = relative_path.replace("\\", '/')
                    all_files.append(relative_path)
                    lua_index = relative_path.find('/lua/')
                    if lua_index == -1:
                        lua_index = relative_path.find('/Lua/')
                    target_path.append(relative_path[lua_index+5: len(relative_path)])

        return all_files, target_path
    else:
        # assets
        abs_path = "UnityProj/{}".format(lua_path)
        for root, subdirs, files in os.walk(abs_path):
            for file_path in files:
                if file_path.endswith('.lua'):
                    relative_path = os.path.join(root, file_path)
                    relative_path = relative_path.replace("\\", '/')
                    all_files.append(relative_path)
                    lua_index = relative_path.find('/lua/')
                    if lua_index == -1:
                        lua_index = relative_path.find('/Lua/')
                    target_path.append(relative_path[lua_index+5: len(relative_path)])

    return all_files, target_path

# if __name__ == "__main__":
#     if not check_job_newest():
#         log("当前Job已经有更新的流水线准备运行了，以最新流水线的资源为准，因此退出...")
#         exit(0)
#     try:
#         mount_path = connect_http_server(env("WIN64_SERVER"))
#         target_path_root = "{}/{}/{}/WinPackageProj_Data/StreamingAssets/aa/StandaloneWindows64/64".format(mount_path, env("CI_PROJECT_PATH"), env("CI_COMMIT_REF_SLUG"))
#         subrules = xmlParse("UnityProj/Assets/astral-local/Editor/LocalSetting/LocalBundleStrategy.xml").documentElement.getElementsByTagName("directory")
#         for rule in subrules:
#             if rule.hasAttribute("schema") and rule.getAttribute("schema") == 'lua':
#                 source_paths, targets_paths = Get_Lua_Files(rule.getAttribute("path"))
#                 for i in range(len(source_paths)):
#                     cp(source_paths[i], os.path.join(target_path_root, targets_paths[i]))
#     finally:
#         disconnect_http_server(env("WIN64_SERVER"))

def pick_lua(path):
    subrules = xmlParse("UnityProj/Assets/astral-local/Editor/LocalSetting/LocalBundleStrategy.xml").documentElement.getElementsByTagName("directory")
    for rule in subrules:
        if rule.hasAttribute("schema") and rule.getAttribute("schema") == 'lua':
            source_paths, targets_paths = Get_Lua_Files(rule.getAttribute("path"))
            for i in range(len(source_paths)):
                cp(source_paths[i], os.path.join(path, targets_paths[i]))
