# coding=utf-8
from ci import *


if __name__ == '__main__':
    if env_bool("ENABLE_SAMPLE_SHADER_VARIANT"):
        cp("customshadervariants_format.yaml", "UnityProj/Assets/GameAssets/shared/urpshaders/customshadervariants_format.yaml")
        cp("customshadervariants.shadervariants", "UnityProj/Assets/GameAssets/shared/urpshaders/customshadervariants.shadervariants")

    buildTarget, platformIndex = getUnityPlatformInfo(env("PLATFORM"))
    return_code = unity(
        buildTarget = buildTarget,
        executeMethod = env("BUILD_PACKAGE_METHOD"),
        isDebug = env("IS_DEBUG"),
        isProfiler = env("IS_PROFILER", env("IS_DEBUG")),
        isDeepProfiler = env("IS_DEEP_PROFILER", env("IS_DEBUG")),
        versionName = env("VERSION_NAME"),
        versionCode = env("VERSION_CODE"),
        isMultithreadedRendering = env("IS_MULTITHREADED_RENDERING"),
        isVulkan = env("IS_VULKAN"),
        isMonoBuild = env("IS_MONO_BUILD", "false"),
        gameConfigUrlType = env("GAMECONFIG_URL_TYPE"),
        gameConfigBranchName = env("CI_COMMIT_REF_NAME"),
        gameConfigLanguageType = env("BUILD_LANG_DEFAULT"),
        gameConfigSupportLanguageType = env("BUILD_LANG"),
        enableSdkLogin = env("ENABLE_SDK_LOGIN")
    )

    logfile = "log.txt"
    error_logs = [
        "IOException: Disk full",
        "Unable to locate Android SDK",
        "Unable to locate Android NDK",
        "Error building Player because scripts had compiler errors",
        "The Process object must have the UseShellExecute property set to false in order to redirect IO streams",
        "ArgumentOutOfRangeException: Index was out of range."
    ]
    for error_log in error_logs:
        if find_log(logfile, error_log):
            error("构建出错: " + error_log)

    exit(return_code)
