// 默认表单配置
LAYOUT_DEFAULF_CONFIG = [
    {
        label: "选择构造机:",
        items: [
            {
                type: "radio",
                name: "select_machine",
                values: [
                    {
                        val: "mac",
                        text: "Mac马蜂窝"
                    },
                    {
                        val: "windows",
                        text: "Win测试机244"
                    }
                ],
                defaultValueKey: "variables[SELECT_MACHINE]"
            }
        ]
    },
    {
        label: "进行的操作:",
        items: [
            {
                type: "checkbox",
                name: "enable_bundle",
                values: [
                    {
                        val: "true",
                        text: "构建AB"
                    }
                ],
                defaultValueKey: "variables[ENABLE_BUNDLE]"
            },
            {
                type: "checkbox",
                name: "enable_hotupdate",
                values: [
                    {
                        val: "true",
                        text: "生成热更包"
                    }
                ],
                defaultValueKey: "variables[ENABLE_HOTUPDATE]"
            },
            {
                type: "checkbox",
                name: "enable_package",
                values: [
                    {
                        val: "true",
                        text: "生成整包"
                    }
                ],
                defaultValueKey: "variables[ENABLE_PACKAGE]"
            },
        ]
    },
    {
        label: "流水线平台:",
        items: [
            {
                type: "checkbox",
                name: "pipeline_platform",
                values: [
                    {
                        val: "android",
                        text: "android"
                    },
                    {
                        val: "ios",
                        text: "ios"
                    },
                    {
                        val: "win64",
                        text: "win64"
                    },
                ],
                defaultValueKey: "variables[PIPELINE_PLATFORM]"
            }
        ]
    },
    {
        label: "Token:",
        items: [
            {
                type: "input:text",
                id: "token",
                name: "token",
                placeholder: "c8ce9313f5acfd3645fd4be3cef54",
                defaultValueKey: "token"
            },
            {
                type: "a",
                href: "##ProjectURL##/-/settings/ci_cd",
                text: "在Settings->CI/CD->Pipeline Trigger中查看"
            }
        ]
    },
    {
        label: "分支:",
        items: [
            {
                type: "select",
                id: "branch_select",
                style: "height: 30px; width: 130px;",
                name: "pipeline_platform",
                onChange: "selectBranchOnChangeFunc()",
                values: [
                    {
                        val: "",
                        text: "---手动输入---"
                    },
                    {
                        val: "develop",
                        text: "develop"
                    }
                ],
                defaultValueKey: "ref"
            },
            {
                type: "input:text",
                id: "branch_input",
                placeholder: "release、hotfix、develop",
                defaultValueKey: "ref"
            },
            {
                type: "text",
                value: "下拉选择或手动输入分支名"
            }
        ]
    },
    {
        label: "资源版本:",
        items: [
            {
                type: "input:text",
                id: "resource_version",
                name: "resource_version",
                placeholder: "5.0.0",
                defaultValueKey: "variables[RESOURCE_VERSION]"
            },
            {
                type: "text",
                value: "用作热更"
            }
        ]
    },
    {
        label: "版本名:",
        items: [
            {
                type: "input:text",
                id: "version_name",
                name: "version_name",
                placeholder: "1.0.2",
                defaultValueKey: "variables[VERSION_NAME]"
            },
            {
                type: "text",
                value: "用作出包的versionName"
            }
        ]
    },
    {
        label: "版本号:",
        items: [
            {
                type: "input:text",
                id: "version_code",
                name: "version_code",
                placeholder: "2",
                defaultValueKey: "variables[VERSION_CODE]"
            },
            {
                type: "text",
                value: "用作出包的versionCode"
            }
        ],
    },
    {
        label: "清除并强打AB:",
        items: [
            {
                type: "radio",
                name: "clean_cache",
                values: [
                    {
                        val: "false",
                        text: "不清除"
                    },
                    {
                        val: "true",
                        text: "清除并强打"
                    }
                ],
                defaultValueKey: "variables[CLEAN_CACHE]"
            }
        ]
    },
    {
        label: "网络配置:",
        items: [
            {
                type: "radio",
                name: "game_config_url_type",
                values: [
                    {
                        val: "0",
                        text: "游戏服务器URL配置0"
                    },
                    {
                        val: "1",
                        text: "游戏服务器URL配置1"
                    },
                    {
                        val: "2",
                        text: "游戏服务器URL配置2"
                    },
                    {
                        val: "3",
                        text: "游戏服务器URL配置3"
                    },
                    {
                        val: "4",
                        text: "游戏服务器URL配置4"
                    },
                ],
                defaultValueKey: "variables[GAMECONFIG_URL_TYPE]"
            }
        ]
    },
    {
        label: "Debug:",
        items: [
            {
                type: "radio",
                name: "is_debug",
                values: [
                    {
                        val: "true",
                        text: "True"
                    },
                    {
                        val: "false",
                        text: "False"
                    },
                ],
                defaultValueKey: "variables[IS_DEBUG]"
            }
        ]
    },
    {
        label: "Profiler:",
        items: [
            {
                type: "radio",
                name: "is_profiler",
                values: [
                    {
                        val: "true",
                        text: "True"
                    },
                    {
                        val: "false",
                        text: "False"
                    },
                ],
                defaultValueKey: "variables[IS_PROFILER]"
            }
        ]
    },
    {
        label: "Deep Profiler:",
        items: [
            {
                type: "radio",
                name: "is_deep_profiler",
                values: [
                    {
                        val: "true",
                        text: "True"
                    },
                    {
                        val: "false",
                        text: "False"
                    },
                ],
                defaultValueKey: "variables[IS_DEEP_PROFILER]"
            }
        ]
    },
    {
        label: "SDK调试:",
        items: [
            {
                type: "radio",
                name: "debug_config",
                values: [
                    {
                        val: "2",
                        text: "外网测试版本"
                    },
                    {
                        val: "3",
                        text: "外网发布版本"
                    },
                ],
                defaultValueKey: "variables[ORG_GRADLE_PROJECT_debugConfig]"
            }
        ]
    },
    {
        expand: false,
        label: "支持语言:",
        items: [
            {
                type: "checkbox",
                name: "build_lang",
                values: [
                    {
                        val: "zh",
                        text: "中文"
                    },
                    {
                        val: "en",
                        text: "英文"
                    },
                    {
                        val: "jp",
                        text: "日文"
                    },
                    {
                        val: "kr",
                        text: "韩文"
                    },
                ],
                defaultValueKey: "variables[BUILD_LANG]"
            }
        ]
    },
    {
        expand: false,
        label: "默认语言:",
        items: [
            {
                type: "radio",
                name: "build_lang_default",
                values: [
                    {
                        val: "zh",
                        text: "中文"
                    },
                    {
                        val: "en",
                        text: "英文"
                    },
                    {
                        val: "jp",
                        text: "日文"
                    },
                    {
                        val: "kr",
                        text: "韩文"
                    },
                ],
                defaultValueKey: "variables[BUILD_LANG_DEFAULT]"
            }
        ]
    },
    {
        expand: false,
        label: "安卓出包类型:",
        items: [
            {
                type: "checkbox",
                name: "assemble_tasks",
                values: [
                    {
                        val: "",
                        text: "百田"
                    },
                ],
                defaultValueKey: "variables[ASSEMBLE_TASKS]"
            }
        ]
    },
    {
        label: "安卓obb包:",
        items: [
            {
                type: "radio",
                name: "split_obb",
                values: [
                    {
                        val: "false",
                        text: "只生成apk"
                    },
                    {
                        val: "true",
                        text: "生成apk和obb包"
                    },
                ],
                defaultValueKey: "variables[SPLIT_OBB]"
            }
        ]
    },
    {
        label: "是否mono包:",
        items: [
            {
                type: "radio",
                name: "is_mono_build",
                values: [
                    {
                        val: "false",
                        text: "False"
                    },
                    {
                        val: "true",
                        text: "True"
                    },
                ],
                defaultValueKey: "variables[IS_MONO_BUILD]"
            }
        ]
    },
    {
        label: "GPU调试:",
        items: [
            {
                type: "radio",
                name: "gpu_debug",
                values: [
                    {
                        val: "true",
                        text: "Enable GPU"
                    },
                    {
                        val: "false",
                        text: "Disable GPU"
                    },
                ],
                defaultValueKey: "variables[GPU_DEBUG]"
            }
        ]
    },
    {
        expand: false,
        label: "iOS SDK:",
        items: [
            {
                type: "radio",
                name: "ios_sdk",
                values: [
                    {
                        val: "OneSDK",
                        text: "百田"
                    },
                ],
                defaultValueKey: "variables[IOS_SDK]"
            }
        ]
    },
    {
        label: "iOS包类型:",
        items: [
            {
                type: "radio",
                name: "is_distribution",
                values: [
                    {
                        val: "false",
                        text: "企业包"
                    },
                    {
                        val: "true",
                        text: "发布包"
                    },
                    {
                        val: "adhoc",
                        text: "adhoc"
                    },
                ],
                defaultValueKey: "variables[IS_DISTRIBUTION]"
            }
        ]
    },
    {
        expand: false,
        label: "清除Library:",
        items: [
            {
                type: "radio",
                name: "clean_library",
                values: [
                    {
                        val: "false",
                        text: "否"
                    },
                    {
                        val: "true",
                        text: "是"
                    },
                ],
                defaultValueKey: "variables[CLEAN_LIBRARY]"
            }
        ]
    },
    {
        label: "采集ShaderVariant:",
        items: [
            {
                type: "radio",
                name: "enable_sample_shader_variant",
                values: [
                    {
                        val: "false",
                        text: "否"
                    },
                    {
                        val: "true",
                        text: "是"
                    },
                ],
                defaultValueKey: "variables[ENABLE_SAMPLE_SHADER_VARIANT]"
            }
        ]
    },
]

// 开发包表单
LAYOUT_CONFIG_FOR_DEV = [
    {
        label: "选择构造机:",
        items: [
            {
                type: "radio",
                name: "select_machine",
                values: [
                    {
                        val: "mac",
                        text: "Mac马蜂窝"
                    },
                    {
                        val: "windows",
                        text: "Win测试机244"
                    }
                ],
                defaultValueKey: "variables[SELECT_MACHINE]"
            }
        ]
    },
    {
        expand: false,
        label: "进行的操作:",
        items: [
            {
                type: "checkbox",
                name: "enable_bundle",
                values: [
                    {
                        val: "true",
                        text: "构建AB"
                    }
                ],
                defaultValueKey: "variables[ENABLE_BUNDLE]"
            },
            {
                type: "checkbox",
                name: "enable_hotupdate",
                values: [
                    {
                        val: "true",
                        text: "生成热更包"
                    }
                ],
                defaultValueKey: "variables[ENABLE_HOTUPDATE]"
            },
            {
                type: "checkbox",
                name: "enable_package",
                values: [
                    {
                        val: "true",
                        text: "生成整包"
                    }
                ],
                defaultValueKey: "variables[ENABLE_PACKAGE]"
            },
        ]
    },
    {
        label: "流水线平台:",
        items: [
            {
                type: "checkbox",
                name: "pipeline_platform",
                values: [
                    {
                        val: "android",
                        text: "android"
                    },
                    {
                        val: "ios",
                        text: "ios"
                    },
                    {
                        val: "win64",
                        text: "win64"
                    },
                ],
                defaultValueKey: "variables[PIPELINE_PLATFORM]"
            }
        ]
    },
    {
        label: "Token:",
        items: [
            {
                type: "input:text",
                id: "token",
                name: "token",
                placeholder: "c8ce9313f5acfd3645fd4be3cef54",
                defaultValueKey: "token"
            },
            {
                type: "a",
                href: "##ProjectURL##/-/settings/ci_cd",
                text: "在Settings->CI/CD->Pipeline Trigger中查看"
            }
        ]
    },
    {
        label: "分支:",
        items: [
            {
                type: "select",
                id: "branch_select",
                style: "height: 30px; width: 130px;",
                name: "pipeline_platform",
                onChange: "selectBranchOnChangeFunc()",
                values: [
                    {
                        val: "",
                        text: "---手动输入---"
                    },
                    {
                        val: "develop",
                        text: "develop"
                    }
                ],
                defaultValueKey: "ref"
            },
            {
                type: "input:text",
                id: "branch_input",
                placeholder: "release、hotfix、develop",
                defaultValueKey: "ref"
            },
            {
                type: "text",
                value: "下拉选择或手动输入分支名"
            }
        ]
    },
    {
        label: "资源版本:",
        items: [
            {
                type: "input:text",
                id: "resource_version",
                name: "resource_version",
                placeholder: "5.0.0",
                defaultValueKey: "variables[RESOURCE_VERSION]"
            },
            {
                type: "text",
                value: "用作热更"
            }
        ]
    },
    {
        label: "版本名:",
        items: [
            {
                type: "input:text",
                id: "version_name",
                name: "version_name",
                placeholder: "1.0.2",
                defaultValueKey: "variables[VERSION_NAME]"
            },
            {
                type: "text",
                value: "用作出包的versionName"
            }
        ]
    },
    {
        label: "版本号:",
        items: [
            {
                type: "input:text",
                id: "version_code",
                name: "version_code",
                placeholder: "2",
                defaultValueKey: "variables[VERSION_CODE]"
            },
            {
                type: "text",
                value: "用作出包的versionCode"
            }
        ],
    },
    {
        label: "清除并强打AB:",
        items: [
            {
                type: "radio",
                name: "clean_cache",
                values: [
                    {
                        val: "false",
                        text: "不清除"
                    },
                    {
                        val: "true",
                        text: "清除并强打"
                    }
                ],
                defaultValueKey: "variables[CLEAN_CACHE]"
            }
        ]
    },
    {
        expand: false,
        label: "网络配置:",
        items: [
            {
                type: "radio",
                name: "game_config_url_type",
                values: [
                    {
                        val: "0",
                        text: "游戏服务器URL配置0"
                    },
                    {
                        val: "1",
                        text: "游戏服务器URL配置1"
                    },
                    {
                        val: "2",
                        text: "游戏服务器URL配置2"
                    },
                    {
                        val: "3",
                        text: "游戏服务器URL配置3"
                    },
                    {
                        val: "4",
                        text: "游戏服务器URL配置4"
                    },
                ],
                defaultValueKey: "variables[GAMECONFIG_URL_TYPE]"
            }
        ]
    },
    {
        label: "Debug:",
        items: [
            {
                type: "radio",
                name: "is_debug",
                values: [
                    {
                        val: "true",
                        text: "True"
                    },
                    {
                        val: "false",
                        text: "False"
                    },
                ],
                defaultValueKey: "variables[IS_DEBUG]"
            }
        ]
    },
    {
        expand: false,
        label: "Profiler:",
        items: [
            {
                type: "radio",
                name: "is_profiler",
                values: [
                    {
                        val: "true",
                        text: "True"
                    },
                    {
                        val: "false",
                        text: "False"
                    },
                ],
                defaultValueKey: "variables[IS_PROFILER]"
            }
        ]
    },
    {
        label: "Deep Profiler:",
        items: [
            {
                type: "radio",
                name: "is_deep_profiler",
                values: [
                    {
                        val: "true",
                        text: "True"
                    },
                    {
                        val: "false",
                        text: "False"
                    },
                ],
                defaultValueKey: "variables[IS_DEEP_PROFILER]"
            }
        ]
    },
    {
        label: "SDK调试:",
        items: [
            {
                type: "radio",
                name: "debug_config",
                values: [
                    {
                        val: "2",
                        text: "外网测试版本"
                    },
                    {
                        val: "3",
                        text: "外网发布版本"
                    },
                ],
                defaultValueKey: "variables[ORG_GRADLE_PROJECT_debugConfig]"
            }
        ]
    },
    {
        expand: false,
        label: "支持语言:",
        items: [
            {
                type: "checkbox",
                name: "build_lang",
                values: [
                    {
                        val: "zh",
                        text: "中文"
                    },
                ],
                defaultValueKey: "variables[BUILD_LANG]"
            }
        ]
    },
    {
        expand: false,
        label: "默认语言:",
        items: [
            {
                type: "radio",
                name: "build_lang_default",
                values: [
                    {
                        val: "zh",
                        text: "中文"
                    },
                ],
                defaultValueKey: "variables[BUILD_LANG_DEFAULT]"
            }
        ]
    },
    {
        expand: false,
        label: "安卓出包类型:",
        items: [
            {
                type: "checkbox",
                name: "assemble_tasks",
                values: [
                    {
                        val: "",
                        text: "百田"
                    },
                ],
                defaultValueKey: "variables[ASSEMBLE_TASKS]"
            }
        ]
    },
    {
        label: "安卓obb包:",
        items: [
            {
                type: "radio",
                name: "split_obb",
                values: [
                    {
                        val: "false",
                        text: "只生成apk"
                    },
                    {
                        val: "true",
                        text: "生成apk和obb包"
                    },
                ],
                defaultValueKey: "variables[SPLIT_OBB]"
            }
        ]
    },
    {
        label: "是否mono包:",
        items: [
            {
                type: "radio",
                name: "is_mono_build",
                values: [
                    {
                        val: "false",
                        text: "False"
                    },
                    {
                        val: "true",
                        text: "True"
                    },
                ],
                defaultValueKey: "variables[IS_MONO_BUILD]"
            }
        ]
    },
    {
        label: "GPU调试:",
        items: [
            {
                type: "radio",
                name: "gpu_debug",
                values: [
                    {
                        val: "true",
                        text: "Enable GPU"
                    },
                    {
                        val: "false",
                        text: "Disable GPU"
                    },
                ],
                defaultValueKey: "variables[GPU_DEBUG]"
            }
        ]
    },
    {
        expand: false,
        label: "iOS SDK:",
        items: [
            {
                type: "radio",
                name: "ios_sdk",
                values: [
                    {
                        val: "OneSDK",
                        text: "百田"
                    },
                ],
                defaultValueKey: "variables[IOS_SDK]"
            }
        ]
    },
    {
        label: "iOS包类型:",
        items: [
            {
                type: "radio",
                name: "is_distribution",
                values: [
                    {
                        val: "false",
                        text: "企业包"
                    },
                    {
                        val: "true",
                        text: "发布包"
                    },
                    {
                        val: "adhoc",
                        text: "adhoc"
                    },
                ],
                defaultValueKey: "variables[IS_DISTRIBUTION]"
            }
        ]
    },
]

// 发布包表单
LAYOUT_CONFIG_FOR_REL = [
    {
        expand: false,
        label: "选择构造机:",
        items: [
            {
                type: "radio",
                name: "select_machine",
                values: [
                    {
                        val: "mac",
                        text: "Mac马蜂窝"
                    },
                    {
                        val: "windows",
                        text: "Win测试机244"
                    }
                ],
                defaultValueKey: "variables[SELECT_MACHINE]"
            }
        ]
    },
    {
        expand: false,
        label: "进行的操作:",
        items: [
            {
                type: "checkbox",
                name: "enable_bundle",
                values: [
                    {
                        val: "true",
                        text: "构建AB"
                    }
                ],
                defaultValueKey: "variables[ENABLE_BUNDLE]"
            },
            {
                type: "checkbox",
                name: "enable_hotupdate",
                values: [
                    {
                        val: "true",
                        text: "生成热更包"
                    }
                ],
                defaultValueKey: "variables[ENABLE_HOTUPDATE]"
            },
            {
                type: "checkbox",
                name: "enable_package",
                values: [
                    {
                        val: "true",
                        text: "生成整包"
                    }
                ],
                defaultValueKey: "variables[ENABLE_PACKAGE]"
            },
        ]
    },
    {
        expand: false,
        label: "流水线平台:",
        items: [
            {
                type: "checkbox",
                name: "pipeline_platform",
                values: [
                    {
                        val: "android",
                        text: "android"
                    },
                    {
                        val: "ios",
                        text: "ios"
                    },
                    {
                        val: "win64",
                        text: "win64"
                    },
                ],
                defaultValueKey: "variables[PIPELINE_PLATFORM]"
            }
        ]
    },
    {
        label: "Token:",
        items: [
            {
                type: "input:text",
                id: "token",
                name: "token",
                placeholder: "c8ce9313f5acfd3645fd4be3cef54",
                defaultValueKey: "token"
            },
            {
                type: "a",
                href: "##ProjectURL##/-/settings/ci_cd",
                text: "在Settings->CI/CD->Pipeline Trigger中查看"
            }
        ]
    },
    {
        label: "分支:",
        items: [
            {
                type: "select",
                id: "branch_select",
                style: "height: 30px; width: 130px;",
                name: "pipeline_platform",
                onChange: "selectBranchOnChangeFunc()",
                values: [
                    {
                        val: "",
                        text: "---手动输入---"
                    },
                    {
                        val: "develop",
                        text: "develop"
                    }
                ],
                defaultValueKey: "ref"
            },
            {
                type: "input:text",
                id: "branch_input",
                placeholder: "release、hotfix、develop",
                defaultValueKey: "ref"
            },
            {
                type: "text",
                value: "下拉选择或手动输入分支名"
            }
        ]
    },
    {
        label: "资源版本:",
        items: [
            {
                type: "input:text",
                id: "resource_version",
                name: "resource_version",
                placeholder: "5.0.0",
                defaultValueKey: "variables[RESOURCE_VERSION]"
            },
            {
                type: "text",
                value: "用作热更"
            }
        ]
    },
    {
        label: "版本名:",
        items: [
            {
                type: "input:text",
                id: "version_name",
                name: "version_name",
                placeholder: "1.0.2",
                defaultValueKey: "variables[VERSION_NAME]"
            },
            {
                type: "text",
                value: "用作出包的versionName"
            }
        ]
    },
    {
        label: "版本号:",
        items: [
            {
                type: "input:text",
                id: "version_code",
                name: "version_code",
                placeholder: "2",
                defaultValueKey: "variables[VERSION_CODE]"
            },
            {
                type: "text",
                value: "用作出包的versionCode"
            }
        ],
    },
    {
        label: "清除并强打AB:",
        items: [
            {
                type: "radio",
                name: "clean_cache",
                values: [
                    {
                        val: "false",
                        text: "不清除"
                    },
                    {
                        val: "true",
                        text: "清除并强打"
                    }
                ],
                defaultValueKey: "variables[CLEAN_CACHE]"
            }
        ]
    },
    {
        label: "网络配置:",
        items: [
            {
                type: "radio",
                name: "game_config_url_type",
                values: [
                    {
                        val: "0",
                        text: "游戏服务器URL配置0"
                    },
                    {
                        val: "1",
                        text: "游戏服务器URL配置1"
                    },
                    {
                        val: "2",
                        text: "游戏服务器URL配置2"
                    },
                    {
                        val: "3",
                        text: "游戏服务器URL配置3"
                    },
                    {
                        val: "4",
                        text: "游戏服务器URL配置4"
                    },
                ],
                defaultValueKey: "variables[GAMECONFIG_URL_TYPE]"
            }
        ]
    },
    {
        label: "Debug:",
        items: [
            {
                type: "radio",
                name: "is_debug",
                values: [
                    {
                        val: "true",
                        text: "True"
                    },
                    {
                        val: "false",
                        text: "False"
                    },
                ],
                defaultValueKey: "variables[IS_DEBUG]"
            }
        ]
    },
    {
        expand: false,
        label: "Profiler:",
        items: [
            {
                type: "radio",
                name: "is_profiler",
                values: [
                    {
                        val: "true",
                        text: "True"
                    },
                    {
                        val: "false",
                        text: "False"
                    },
                ],
                defaultValueKey: "variables[IS_PROFILER]"
            }
        ]
    },
    {
        label: "Deep Profiler:",
        items: [
            {
                type: "radio",
                name: "is_deep_profiler",
                values: [
                    {
                        val: "true",
                        text: "True"
                    },
                    {
                        val: "false",
                        text: "False"
                    },
                ],
                defaultValueKey: "variables[IS_DEEP_PROFILER]"
            }
        ]
    },
    {
        label: "SDK调试:",
        items: [
            {
                type: "radio",
                name: "debug_config",
                values: [
                    {
                        val: "2",
                        text: "外网测试版本"
                    },
                    {
                        val: "3",
                        text: "外网发布版本"
                    },
                ],
                defaultValueKey: "variables[ORG_GRADLE_PROJECT_debugConfig]"
            }
        ]
    },
    {
        expand: false,
        label: "支持语言:",
        items: [
            {
                type: "checkbox",
                name: "build_lang",
                values: [
                    {
                        val: "zh",
                        text: "中文"
                    },
                ],
                defaultValueKey: "variables[BUILD_LANG]"
            }
        ]
    },
    {
        expand: false,
        label: "默认语言:",
        items: [
            {
                type: "radio",
                name: "build_lang_default",
                values: [
                    {
                        val: "zh",
                        text: "中文"
                    },
                ],
                defaultValueKey: "variables[BUILD_LANG_DEFAULT]"
            }
        ]
    },
    {
        expand: false,
        label: "安卓出包类型:",
        items: [
            {
                type: "checkbox",
                name: "assemble_tasks",
                values: [
                    {
                        val: "",
                        text: "百田"
                    },
                ],
                defaultValueKey: "variables[ASSEMBLE_TASKS]"
            }
        ]
    },
    {
        label: "安卓obb包:",
        items: [
            {
                type: "radio",
                name: "split_obb",
                values: [
                    {
                        val: "false",
                        text: "只生成apk"
                    },
                    {
                        val: "true",
                        text: "生成apk和obb包"
                    },
                ],
                defaultValueKey: "variables[SPLIT_OBB]"
            }
        ]
    },
    {
        label: "是否mono包:",
        items: [
            {
                type: "radio",
                name: "is_mono_build",
                values: [
                    {
                        val: "false",
                        text: "False"
                    },
                    {
                        val: "true",
                        text: "True"
                    },
                ],
                defaultValueKey: "variables[IS_MONO_BUILD]"
            }
        ]
    },
    {
        label: "GPU调试:",
        items: [
            {
                type: "radio",
                name: "gpu_debug",
                values: [
                    {
                        val: "true",
                        text: "Enable GPU"
                    },
                    {
                        val: "false",
                        text: "Disable GPU"
                    },
                ],
                defaultValueKey: "variables[GPU_DEBUG]"
            }
        ]
    },
    {
        expand: false,
        label: "iOS SDK:",
        items: [
            {
                type: "radio",
                name: "ios_sdk",
                values: [
                    {
                        val: "OneSDK",
                        text: "百田"
                    },
                ],
                defaultValueKey: "variables[IOS_SDK]"
            }
        ]
    },
    {
        label: "iOS包类型:",
        items: [
            {
                type: "radio",
                name: "is_distribution",
                values: [
                    {
                        val: "false",
                        text: "企业包"
                    },
                    {
                        val: "true",
                        text: "发布包"
                    },
                    {
                        val: "adhoc",
                        text: "adhoc"
                    },
                ],
                defaultValueKey: "variables[IS_DISTRIBUTION]"
            }
        ]
    },
]

// 默认数据
CIFORM_DEFAULT_VALUE = {
    "token": "xxxxxxxxxxxxx",
    "ref": "develop",
    "variables[SELECT_MACHINE]": "mac",
    "variables[ENABLE_BUNDLE]": "true",
    "variables[ENABLE_HOTUPDATE]": "true",
    "variables[ENABLE_PACKAGE]": "true",
    "variables[PIPELINE_PLATFORM]": "android,win64",
    "variables[RESOURCE_VERSION]": "",
    "variables[VERSION_NAME]": "",
    "variables[VERSION_CODE]": "",
    "variables[CLEAN_CACHE]": "false",
    "variables[GAMECONFIG_URL_TYPE]": "0",
    "variables[IS_DEBUG]": "true",
    "variables[IS_PROFILER]": "true",
    "variables[IS_DEEP_PROFILER]": "true",
    "variables[ORG_GRADLE_PROJECT_debugConfig]": "2",
    "variables[BUILD_LANG]": "zh",
    "variables[BUILD_LANG_DEFAULT]": "zh",
    "variables[ANDROID_SDK]": "sdkone",
    "variables[ASSEMBLE_TASKS]": "",
    "variables[SPLIT_OBB]": "false",
    "variables[IS_MONO_BUILD]": "false",
    "variables[GPU_DEBUG]": "true",
    "variables[IOS_SDK]": "OneSDK",
    "variables[IS_DISTRIBUTION]": "false",
    "variables[CLEAN_LIBRARY]": "false",
    "variables[ENABLE_SAMPLE_SHADER_VARIANT]": "false",
}

// 预设列表
CIFORM_LIST = [
    {
        name: "配置一",
        layout: LAYOUT_DEFAULF_CONFIG,
        defaultValueObj: CIFORM_DEFAULT_VALUE
    },
    {
        name: "配置二",
        layout: LAYOUT_CONFIG_FOR_DEV,
        defaultValueObj: CIFORM_DEFAULT_VALUE
    },
    {
        name: "配置三",
        layout: LAYOUT_CONFIG_FOR_REL,
        defaultValueObj: CIFORM_DEFAULT_VALUE
    },
]