$ErrorActionPreference = "Stop";

if ("${ANDROID_SDK}" -eq "ubeejoy") {
    $BUNDLE_ID="com.ubj.alx.gp"
    if ("assembleGpEnDevDebug" -eq "${ASSEMBLE_TASK}") {
        $APK_PATH="./ubeejoy/build/outputs/apk/gpEnDev/debug/ubeejoy-gpEnDev-debug.apk"
        $DEBUG_PREFIX="opendebug"
    } else {
        $APK_PATH="./ubeejoy/build/outputs/apk/gpEnProduct/release/ubeejoy-gpEnProduct-release.apk"
        $DEBUG_PREFIX="closedebug"
    }
} else {
    $BUNDLE_ID="com.baitian.alx.alxsy.bt"
    if ("true" -eq "${IS_DEBUG}") {
        $APK_PATH="./sdkone/build/outputs/apk/debug/sdkone-debug.apk"
        $DEBUG_PREFIX="opendebug"
    } else {
        $APK_PATH="./sdkone/build/outputs/apk/release/sdkone-release.apk"
        $DEBUG_PREFIX="closedebug"
    }
}

if (${GAMECONFIG_URL_TYPE} -eq 0) {
    $CONFIG="inner_dev"
} elseif (${GAMECONFIG_URL_TYPE} -eq 1) {
    $CONFIG="onlinetest"
} elseif (${GAMECONFIG_URL_TYPE} -eq 2) {
    $CONFIG="online"
} elseif (${GAMECONFIG_URL_TYPE} -eq 3) {
    $CONFIG="inner_test"
} elseif (${GAMECONFIG_URL_TYPE} -eq 4) {
    $CONFIG="prerelease"
}

$MAIN_OBB_NAME="${CI_COMMIT_REF_SLUG}/${CONFIG}/main.versionCode.${BUNDLE_ID}.obb"
$PATCH_OBB_NAME="${CI_COMMIT_REF_SLUG}/${CONFIG}/patch.versionCode.${BUNDLE_ID}.obb"
$APK_NAME="${CI_COMMIT_REF_SLUG}_${CONFIG}.apk"
$DIR_NAME="/home/site/play/${CI_COMMIT_REF_SLUG}/${CONFIG}/"

pscp.exe -batch -ssh -i C:/gitlab-runner/id_rsa.ppk -r ${APK_PATH} root@10.17.1.108:/home/package_backup/${DEBUG_PREFIX}-${CI_COMMIT_REF_SLUG}-${RESOURCE_VERSION}_${VERSION_NAME}_${GAMECONFIG_URL_TYPE}_${CI_PIPELINE_ID}.apk
if ($LASTEXITCODE -ne 0) {exit 1;}
pscp.exe -batch -ssh -i C:/gitlab-runner/id_rsa.ppk -r ${APK_PATH} root@10.17.1.107:/home/site/play/${APK_NAME}
if ($LASTEXITCODE -ne 0) {exit 1;}

if (("${SPLIT_OBB}" -eq "true") -and ("${FORCE_OLD_OBB}" -eq "false")) {
    plink.exe -batch -ssh -i C:/gitlab-runner/id_rsa.ppk root@10.17.1.107 "[ -d ${DIR_NAME} ] && echo ok || mkdir -p ${DIR_NAME}"
    if ($LASTEXITCODE -ne 0) {exit 1;}
    echo ${MAIN_OBB_NAME}
    pscp.exe -batch -ssh -i C:/gitlab-runner/id_rsa.ppk -r ./${CI_PIPELINE_ID}.obb root@10.17.1.108:/home/package_backup/obb-main-${CI_COMMIT_REF_SLUG}-${RESOURCE_VERSION}_${VERSION_NAME}_${GAMECONFIG_URL_TYPE}_${CI_PIPELINE_ID}.obb
    if ($LASTEXITCODE -ne 0) {exit 1;}
    pscp.exe -batch -ssh -i C:/gitlab-runner/id_rsa.ppk -r ./${CI_PIPELINE_ID}.obb root@10.17.1.107:/home/site/play/${MAIN_OBB_NAME}
    if ($LASTEXITCODE -ne 0) {exit 1;}
	
	if (Test-Path "./patch_${CI_PIPELINE_ID}.obb") 
	{
		echo ${PATCH_OBB_NAME}
		pscp.exe -batch -ssh -i C:/gitlab-runner/id_rsa.ppk -r ./patch_${CI_PIPELINE_ID}.obb root@10.17.1.108:/home/package_backup/obb-patch-${CI_COMMIT_REF_SLUG}-${RESOURCE_VERSION}_${VERSION_NAME}_${GAMECONFIG_URL_TYPE}_${CI_PIPELINE_ID}.obb
		if ($LASTEXITCODE -ne 0) {exit 1;}
		pscp.exe -batch -ssh -i C:/gitlab-runner/id_rsa.ppk -r ./patch_${CI_PIPELINE_ID}.obb root@10.17.1.107:/home/site/play/${PATCH_OBB_NAME}
		if ($LASTEXITCODE -ne 0) {exit 1;}
	}
}
