# coding=utf-8
import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from ci import *

bugly_dir = "./ipaout/output/app.xcarchive/dSYMs"
if "retry-bugly" not in env("CI_JOB_NAME") and not env_bool("IS_DEBUG") and env("CI_PIPELINE_SOURCE") != "schedule":
    mkdir(ci_home("bugly-symbol", env("CI_PROJECT_PATH")))
    rm(ci_home("bugly-symbol", env("CI_PROJECT_PATH"), env("PLATFORM") + env("CI_COMMIT_REF_SLUG")))
    cp(bugly_dir, ci_home("bugly-symbol", env("CI_PROJECT_PATH"), env("PLATFORM") + env("CI_COMMIT_REF_SLUG")))
elif "retry-bugly" in env("CI_JOB_NAME"):
    rm(bugly_dir)
    cp(ci_home("bugly-symbol", env("CI_PROJECT_PATH"), env("PLATFORM") + env("CI_COMMIT_REF_SLUG")), bugly_dir)

bugly_env = {
    "OneSDK": ["BUGLY_APP_ID_IOS","BUGLY_APP_KEY_IOS"],
    "MSDK": ["MSDK_BUGLY_APP_ID_IOS","MSDK_BUGLY_APP_KEY_IOS"],
    "UbeejoySDK": ["UBJ_BUGLY_APP_ID_IOS","UBJ_BUGLY_APP_KEY_IOS"],
    "UbeejoySDKKR": ["UBJ_KR_BUGLY_APP_ID_IOS","UBJ_KR_BUGLY_APP_KEY_IOS"],
    "EfunSDK": ["EFUN_BUGLY_APP_ID_IOS","EFUN_BUGLY_APP_KEY_IOS"],
    "EfunSDKJP": ["EFUN_JP_BUGLY_APP_ID_IOS","EFUN_JP_BUGLY_APP_KEY_IOS"],
    "YouzuSDKKR": ["YOUZU_KR_BUGLY_APP_ID_IOS","YOUZU_KR_BUGLY_APP_KEY_IOS"]
}

ios_sdk = env("IOS_SDK")
PlistBuddy("./ipaout/output/app.xcarchive/Info.plist", "Print :ApplicationProperties:CFBundleIdentifier", stdout = "ci_bundle_id.txt")
bundle_id = cat("ci_bundle_id.txt", stdout = False)
bugly_id = env(bugly_env[ios_sdk][0])
bugly_key = env(bugly_env[ios_sdk][1])
upload_bugly(bugly_dir, "IOS", bugly_id, bugly_key, env("VERSION_NAME"), bundle_id)
