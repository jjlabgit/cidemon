# coding=utf-8

import os
import sys
import hashlib
import datetime
import yaml
import re
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from ci import *
import luapicker
import update_ab
import update_exe

def GetFileMd5(file_path):
    """
    文件md5
    :param file_path:文件路径
    :return:md5
    """
    if not os.path.isfile(file_path):
        return ''
    filehash = hashlib.md5()
    with open(file_path, mode='rb') as fi:
        while True:
            b = fi.read(8096)
            if not b:
                break
            filehash.update(b)
        fi.close()

    return filehash.hexdigest()

def versioning(dir_name, remote_path):
    fileversion_path = os.path.join(dir_name, "FILE_VERSION.txt")
    version_path = os.path.join(dir_name, "VERSION.txt")
    rm(fileversion_path, version_path)

    # 生成版本时间
    ip = re.split(r'[:@/,]+', env("WIN64_SERVER"))[2]
    port = re.split(r'[:@/,]+', env("WIN64_SERVER"))[4]
    version = {
        'version_data': datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
        'remote_url': '{}/{}'.format('http://{}:{}'.format(ip, port), remote_path)
    }
    with open(version_path, mode='w+') as fo:
        fo.write(yaml.dump(version))
        fo.flush()
        fo.close()

    # 生成version hash5
    md5s = ''
    for root, subs, files in os.walk(dir_name):
        for file_name in files:
            file_path = os.path.join(root, file_name)
            file_path = file_path.replace('\\', '/')
            dir_name = dir_name.replace('\\', '/')
            key_str = file_path.replace(dir_name, '', 1)
            if len(md5s) == 0:
                md5s = "{}=>{}".format(key_str, GetFileMd5(file_path))
            else:
                md5s = "{}\n{}=>{}".format(md5s, key_str, GetFileMd5(file_path))

    with open(fileversion_path, mode='w+') as fo:
        fo.write(md5s)
        fo.flush()
        fo.close()

if __name__ == "__main__":
    # if not check_job_newest(newer_job_status = ["created", "pending"], job_name = "win64:versioning"):
    #     log("当前Job已经有更新的流水线准备运行了，以最新流水线的资源为准，因此退出...")
    #     exit(0)
    if not os.path.exists("WinPackageProj"):
        update_exe.main()


    rm("WinPackageProj/WinPackageProj_Data/StreamingAssets")
    zip_py(env("CI_PIPELINE_ID") + ".zip", "WinPackageProj/*")

    if not os.path.exists("UnityProj/Library/com.unity.addressables/aa/Windows/StandaloneWindows64"):
        update_ab.main()
    rm("UnityProj/Library/com.unity.addressables/aa/Windows/StandaloneWindows64/64")
    luapicker.pick_lua("UnityProj/Library/com.unity.addressables/aa/Windows/StandaloneWindows64/64")
    zip_py(env("CI_PIPELINE_ID") + ".zip", "UnityProj/Library/com.unity.addressables/aa/Windows/*", parent_dir = "WinPackageProj_Data/StreamingAssets/aa")

    start = time.time()
    try:
        mount_path = connect_http_server(env("WIN64_SERVER"))
        # versioning(os.path.join(mount_path, env("CI_PROJECT_PATH"), env("CI_COMMIT_REF_SLUG")), "{}/{}".format(env("CI_PROJECT_PATH"), env("CI_COMMIT_REF_SLUG")))
        target_path = "{}/{}/{}".format(mount_path, env("CI_PROJECT_PATH"), env("CI_COMMIT_REF_SLUG"))
        
        cp(env("CI_PIPELINE_ID") + ".zip", target_path)
        write_file("{}/{}.log".format(target_path, env("CI_PIPELINE_ID")), env("CI_COMMIT_MESSAGE"), mode = "w", newline = False)
    finally:
        disconnect_http_server(env("WIN64_SERVER"))
    end = time.time()
    log("传输用时：" + str(end - start) + "秒")

    mkdir("WinPackageProj/WinPackageProj_Data/StreamingAssets/aa")
    cp("UnityProj/Library/com.unity.addressables/aa/Windows/*", "WinPackageProj/WinPackageProj_Data/StreamingAssets/aa")

    # 上传到repo.bt
    d = gitlab_api("projects/{}/pipelines/{}".format(env("CI_PROJECT_ID"), env("CI_PIPELINE_ID")))["created_at"]
    pipeline_date = "{}-{}-{}-{}".format(d[5:7], d[8:10], d[11:13], d[14:16])
    backupURL = env("GAME_ID") + "/win64/backup/" + "_".join([env("CI_PIPELINE_ID"), env("CI_COMMIT_REF_SLUG"), pipeline_date]) + ".zip"
    upload_nexus(env("CI_PIPELINE_ID") + ".zip", backupURL)
