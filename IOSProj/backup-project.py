# coding=utf-8
import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from ci import *

backup_dir = ci_home("ci-xcode-project", env("CI_PROJECT_PATH"), env("CI_COMMIT_REF_SLUG"))
if env_bool("IOS_BACKUP_PROJECT"):
    rm(backup_dir)
    cp(".", backup_dir)
