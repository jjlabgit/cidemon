$ErrorActionPreference = "Stop";

& 'C:\Program Files\7-Zip\7z.exe' a -tzip "${PLATFORM}-${CI_COMMIT_REF_SLUG}-windows.zip" ./UnityProj/AssetBundles/${PLATFORM};
if ($LASTEXITCODE -ne 0) {exit 1;}

$SECURE_PWD = ConvertTo-SecureString ${UPLOAD_PASSWORD} -AsPlainText -Force;
$CREDS = New-Object System.Management.Automation.PSCredential (${UPLOAD_USER}, ${SECURE_PWD});
Upload-File -Url "http://repo.bt/repository/${GAME_ID}/backup/hotupdate-base/${PLATFORM}-${CI_COMMIT_REF_SLUG}-windows.zip" -File ${CI_PROJECT_DIR}/${PLATFORM}-${CI_COMMIT_REF_SLUG}-windows.zip -Credential $CREDS;

if (!(Test-Path C:/gitlab-runner/hotupdate/${CI_PROJECT_PATH}/${CI_COMMIT_REF_SLUG}/)){
    New-Item C:/gitlab-runner/hotupdate/${CI_PROJECT_PATH}/${CI_COMMIT_REF_SLUG}/ -ItemType Directory;
}
if (Test-Path C:/gitlab-runner/hotupdate/${CI_PROJECT_PATH}/${CI_COMMIT_REF_SLUG}/${PLATFORM}-lasttime) {
    rm -r -Force C:/gitlab-runner/hotupdate/${CI_PROJECT_PATH}/${CI_COMMIT_REF_SLUG}/${PLATFORM}-lasttime
}
if (Test-Path C:/gitlab-runner/hotupdate/${CI_PROJECT_PATH}/${CI_COMMIT_REF_SLUG}/${PLATFORM}){
    mv -Force C:/gitlab-runner/hotupdate/${CI_PROJECT_PATH}/${CI_COMMIT_REF_SLUG}/${PLATFORM}/ C:/gitlab-runner/hotupdate/${CI_PROJECT_PATH}/${CI_COMMIT_REF_SLUG}/${PLATFORM}-lasttime/
}
cp -R -Force ./UnityProj/AssetBundles/${PLATFORM}/ C:/gitlab-runner/hotupdate/${CI_PROJECT_PATH}/${CI_COMMIT_REF_SLUG}/${PLATFORM}/
