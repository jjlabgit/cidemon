# coding=utf-8
import os
import sys
from xml.dom.minidom import parse as xmlParse
import json
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from ci import *

# 改文件是使用unity打包exe工程，将打包产出的资源拷贝到更新目录下
# 但是需要排除一下目录 WinPackageProj_Data/StreamingAssets,
# 这个目录存放的是ab和lua的资源，因为这个导出exe并不包含导出ab和lua

def main():
    buildTarget, platformIndex = getUnityPlatformInfo("win64")
    return_code = unity(
        buildTarget = buildTarget,
        executeMethod = "Astral.Core.AutoPackage.AutoWindowExe"
    )

    logfile = "log.txt"
    error_logs = [
        "IOException: Disk full",
        "Error building Player because scripts had compiler errors",
        "The Process object must have the UseShellExecute property set to false in order to redirect IO streams"
    ]
    for error_log in error_logs:
        if find_log(logfile, error_log):
            error("构建出错: " + error_log)

    if return_code != 0:
        exit(return_code)


if __name__ == "__main__":
    # if not check_job_newest():
    #     log("当前Job已经有更新的流水线准备运行了，以最新流水线的资源为准，因此退出...")
    #     exit(0)
    main()
    rm("WinPackageProj/WinPackageProj_Data/StreamingAssets")

    # try:
    #     mount_path = connect_http_server(env("WIN64_SERVER"))
    #     win_package_proj = "./WinPackageProj"
    #     need_move_files = ['UnityCrashHandler64.exe', 'UnityPlayer.dll', 'WinPackageProj.exe', 'WinPixEventRuntime.dll', "MonoBleedingEdge"]
    #     for element in ls(win_package_proj + '/WinPackageProj_Data'):
    #         if element != 'StreamingAssets':
    #             need_move_files.append('WinPackageProj_Data/' + element)
    #     for i in range(len(need_move_files)):
    #         target_path = os.path.join(mount_path, env("CI_PROJECT_PATH"), env("CI_COMMIT_REF_SLUG"), need_move_files[i])
    #         source_path = os.path.join(win_package_proj, need_move_files[i])
    #         rm(target_path)
    #         cp(source_path, target_path)

    # finally:
    #     disconnect_http_server(env("WIN64_SERVER"))
