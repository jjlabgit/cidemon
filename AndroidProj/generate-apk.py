# coding=utf-8
import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import re
from ci import *

# 删除其他语言的AB资源
lang_dir = "./libgame/src/main/assets/aa/Android/language_assets_language"
build_lang = env("BUILD_LANG", "all")
if os.path.isdir(lang_dir) and build_lang != "all":
    for language in ls(lang_dir):
        if language not in build_lang.split(","):
            rm(os.path.join(lang_dir, language))

# 拆分libgame/src/main/assets/Android下要打进obb包的AB资源
# obb-keep-list存放在"AndroidProj/${ANDROID_SDK}/"或"AndroidProj/"(缺省)目录下，文件中定义的文件或文件夹打进apk包，其他则打进obb包
# obb-keep-list不支持通配符
set_env("ORG_GRADLE_PROJECT_btObbSize", "0")
if env_bool("SPLIT_OBB"):
    log("拆分OBB资源文件")
    if os.path.exists(os.path.join(env("ANDROID_SDK"), "obb-keep-list")):
        obb_keep_list = os.path.join(env("ANDROID_SDK"), "obb-keep-list")
    elif os.path.exists("./obb-keep-list"):
        obb_keep_list = "./obb_keep_list"
    else:
        error("配置文件obb-keep-list不存在！正在退出...")

    mkdir("./assets")
    mv("./libgame/src/main/assets/Android","./assets/Android")
    for data in cat(obb_keep_list, stdout = False, mode = "list"):
        if not os.path.exists(data):
            log("警告：obb-keep-list中配置的AndroidProj/libgame/src/main/assets/Android/" + data + "文件或文件夹不存在！", "red")
        else:
            mkdir(dirname(os.path.join("./libgame/src/main/assets/Android/", data)))
            mv(os.path.join("./assets/Android", data), os.path.join("./libgame/src/main/assets/Android/", data))
    build_id = re.compile(r'[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}').findall(find_log("unity.build-id")[0])
    touch(build_id)
    obbname = env("CI_PIPELINE_ID") + ".obb"
    zip_py(obbname, "./assets")
    set_env("ORG_GRADLE_PROJECT_btObbSize", "X_" + str(os.path.getsize(obbname)))
    set_env("ORG_GRADLE_PROJECT_btObbVersion", env("VERSION_CODE"))

log("出包")
shell({"Darwin": "chmod +x ./gradlew"})
debug_release = "Debug" if env_bool("IS_DEBUG") else "Release"
assemble_tasks = env("ASSEMBLE_TASKS", "") if env("ASSEMBLE_TASKS") != "null" else ""
gradle_task = ""
# assemble可以为空字符串
for assemble in assemble_tasks.split(","):
    gradle_task = gradle_task + " :" + env("ANDROID_SDK") + ":assemble" + assemble + debug_release
shell({
    "Darwin": "./gradlew clean " + gradle_task + " --stacktrace --no-daemon",
    "Windows": "./gradlew.bat clean " + gradle_task + " --stacktrace --no-daemon -g 'C:/gitlab-runner/gradle-cache';"
})

# 将libgame/src/main/assets/Android目录复原
if env_bool("SPLIT_OBB"):
    mv("./assets/Android/*", "./libgame/src/main/assets/Android")
