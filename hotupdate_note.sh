#!/bin/sh
set -e

# 到缓存目录中获取上次打的ab缓存(ab以zip的形式存在),执行解压操作(打完ab压缩，热更时又解压对比，浪费时间)
if [[ "${HOTUPDATE_TYPE}" == "remote" ]]; then
    echo "\x1b[32;1mDownloading ${PLATFORM}-${CI_COMMIT_REF_SLUG}.zip ...\x1b[0;m"
    curl -OkLs http://repo.bt/repository/${GAME_ID}/backup/hotupdate-base/${PLATFORM}-${CI_COMMIT_REF_SLUG}.zip
    if [[ "$(file -b ${PLATFORM}-${CI_COMMIT_REF_SLUG}.zip | grep 'Zip')" == "" ]]; then
        echo $'\x1b[32;1mrepo.bt上不存在该分支的热更文件！\x1b[0;m'
        exit 1
    fi
    unzip -qo ${PLATFORM}-${CI_COMMIT_REF_SLUG}.zip -d hotupdate/
    HOTUPDATE_BASE=${CI_PROJECT_DIR}/hotupdate/UnityProj/AssetBundles/${PLATFORM}
else
    # 如果没有缓存的，但是有另外一个资源目录，则从这个目录中拷贝资源
    HOTUPDATE_BASE=~/hotupdate/${CI_PROJECT_PATH}/${CI_COMMIT_REF_SLUG}/${PLATFORM}
    if [ ! -d ${HOTUPDATE_BASE} ]; then
        echo $'\x1b[31;1m未发现前一次构建缓存\x1b[0;m'
        mkdir -p ${HOTUPDATE_BASE}
        cp -Rf UnityProj/AssetBundles/${PLATFORM}/* ${HOTUPDATE_BASE}
        exit 0
    fi
fi

# 删除其他语言的AB资源
if [[ "${BUILD_LANG}" != "all" ]]; then
    LANG_DIR=UnityProj/AssetBundles/${PLATFORM}/language
    for LANGUAGE in $(ls ${LANG_DIR} 2>/dev/null); do
        if [[ ! ${BUILD_LANG} =~ (^|,)${LANGUAGE}($|,) ]];then
            rm -rf ${LANG_DIR}/${LANGUAGE}
            # 为了让filemanifest.xml不显示其他语言的变动，将hotupdate中保存的language目录其他语言目录复制到CI项目中，再进行热更文件对比
            cp -Rf ${HOTUPDATE_BASE}/language/${LANGUAGE} ${LANG_DIR}/${LANGUAGE} 2>/dev/null || true
        fi
    done
fi

IGNORE_DIR=(${HOTUPDATE_IGNORE_DIRS//,/ })
for DIR in ${IGNORE_DIR[@]};do
    rm -rf UnityProj/AssetBundles/${PLATFORM}/${DIR}
    cp -Rf ${HOTUPDATE_BASE}/${DIR} UnityProj/AssetBundles/${PLATFORM}/${DIR} 2>/dev/null || true
done

# 生成 filemanifest.xml 记录需要打热更的ab文件
# 挑选热更文件
cd UnityProj/AssetBundles
#基础路径 目前检测的文件 输出路径
python ../../pick-hotupdate-files.py ${HOTUPDATE_BASE} ./${PLATFORM}/ ../../ ${IGNORE_MANIFEST_DIFF}
cd ${CI_PROJECT_DIR}

# 删除manifest，将AB移动到根目录，生成热更文件压缩包
if [ ! -d ./${PLATFORM} ]; then
    echo $'\x1b[31;1m没有需要热更的文件\x1b[0;m'
    exit 0
fi
find ./${PLATFORM} -name "*.manifest" -delete
# 删除空文件夹
while [[ $(find ./${PLATFORM} -type d -empty) != "" ]]; do 
    find ./${PLATFORM} -type d -empty -exec rmdir {} \; 2>/dev/null || true
done

mv ./${PLATFORM}/* ./
