# coding=utf-8
import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from ci import *

# def rm_exclude(path, exclude = []):
#     if "*" in path:
#         error("")
#     for element in ls(path):
#         exclude_flag = False
#         for exclude_element in exclude:
#             if element == exclude_element:
#                 exclude_flag = True
#                 break
#         if not exclude_flag:
#             rm(os.path.join(path, element))

# def cp_exclude(src, dest, exclude = []):
#     if "*" in src or "*" in dest:
#         error("")
#     mkdir(dest)
#     for element in ls(src):
#         exclude_flag = False
#         for exclude_element in exclude:
#             if element == exclude_element:
#                 exclude_flag = True
#                 break
#         if not exclude_flag:
#             cp(os.path.join(src, element), os.path.join(dest, element))

def main():
    buildTarget, platformIndex = getUnityPlatformInfo("win64")

    return_code = unity(
        buildTarget = buildTarget,
        executeMethod = "Astral.Core.AutoPackage.AutoBuildAssetBundle",
        platformIndex = platformIndex,
        isForceRebuild = "false",
        enableEncodeLua = "false",
        bundleNaming = 0,
        luaBundle = "false"
    )

    logfile = "log.txt"
    error_logs = [
        # 此处不能输入中文
        "Failed executing selected SpritePackerPolicy.ArgumentException",
        "Receiving unhandled NULL exception",
        "EditorApplicationIsCompiling10086",
        "Failed to compile player scripts",
        "Build Task WriteSerializedFiles failed with exception",
        "error! Subprogram failed",
        "=>Object reference not set to an instance of an object"
    ]
    for error_log in error_logs:
        if find_log(logfile, error_log):
            error("构建出错: " + error_log)
    if return_code != 0:
        exit(return_code)

if __name__ == "__main__":
    # if not check_job_newest():
    #     log("当前Job已经有更新的流水线准备运行了，以最新流水线的资源为准，因此退出...")
    #     exit(0)

    main()

    # log('copying assetbundle!')
    # try:
    #     mount_path = connect_http_server(env("WIN64_SERVER"))
    #     res_path = 'UnityProj/Library/com.unity.addressables/aa/Windows'
    #     target_path = "{}/{}/{}/WinPackageProj_Data/StreamingAssets/aa".format(mount_path, env("CI_PROJECT_PATH"), env("CI_COMMIT_REF_SLUG"))
    #     log(res_path + '==>' + target_path, "white")

    #     rm_exclude(target_path, exclude = ["StandaloneWindows64"])
    #     rm_exclude(os.path.join(target_path, "StandaloneWindows64"), exclude = ["64"])

    #     cp_exclude(res_path, target_path, exclude = ["StandaloneWindows64"])
    #     cp_exclude(os.path.join(res_path, "StandaloneWindows64"), os.path.join(target_path, "StandaloneWindows64"), exclude = ["64"])
    # finally:
    #     disconnect_http_server(env("WIN64_SERVER"))
    # log('process ab finish!!')
