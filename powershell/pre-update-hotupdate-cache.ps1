$ErrorActionPreference = "Stop";

$GAME_AUDIO="UnityProj/AssetBundles/${PLATFORM}/gameaudio"
$GAME_VIDEO="UnityProj/AssetBundles/${PLATFORM}/gamevideo"
if (!(Test-Path $GAME_AUDIO)){ New-Item $GAME_AUDIO -ItemType Directory; }
if (!(Test-Path $GAME_VIDEO)){ New-Item $GAME_VIDEO -ItemType Directory; }

cp -R -Force UnityProj/Assets/StreamingAssets/gameaudio/* ${GAME_AUDIO};
cp -R -Force UnityProj/Assets/StreamingAssets/gamevideo/* ${GAME_VIDEO};

Get-ChildItem UnityProj/AssetBundles/${PLATFORM} -recurse -include *.meta | Remove-Item;
