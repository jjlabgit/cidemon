$ErrorActionPreference = "Stop";

$lastLuaVersionPath="C:/gitlab-runner/${Last_Lua_Sha_Folder}/${PLATFORM_LOWER}-${CI_PROJECT_PATH}/${CI_COMMIT_REF_SLUG}/${Last_Lua_Sha_File_Name}"
TempPath="C:/gitlab-runner/${Last_Lua_Sha_Folder}/${CI_PROJECT_PATH}/${PLATFORM_LOWER}-${CI_COMMIT_REF_SLUG}/${Last_Lua_Sha_Temp_File_Name}"
if (!(Test-Path ${TempPath})){
    echo "构建出错，找不到${TempPath}"
    exit 1
}
$newShaVersion=$(Get-Content -Path $TempPath)
echo 'temp shaVersion ${newShaVersion}'
echo 'last shaVersion $(Get-Content -Path $lastLuaVersionPath)'
Set-Content -Path ${lastLuaVersionPath} -Value ${newShaVersion}