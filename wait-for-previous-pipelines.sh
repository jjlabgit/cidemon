#!/bin/bash
set -e

if [ "$ACCESS_TOKEN" == "" ]; then 
    echo $'\x1b[31;1mACCESS_TOKEN没有设置，请前往Secret Variables选项设置\x1b[0;m'
    exit 1; 
fi

# 流水线最多保留15分钟
for (( i=0; i<15; i++ )); do
    curl -kLs --header "PRIVATE-TOKEN:${ACCESS_TOKEN}" "https://gitlab.bt/api/v4/projects/${CI_PROJECT_ID}/pipelines?scope=pending" | jq -r '.[].id' > ./pipelines
    curl -kLs --header "PRIVATE-TOKEN:${ACCESS_TOKEN}" "https://gitlab.bt/api/v4/projects/${CI_PROJECT_ID}/pipelines?scope=running" | jq -r '.[].id' >> ./pipelines
    
    WAIT_FLAG="false"
    for PIPELINE in $(cat < ./pipelines); do
        if [[ $CI_PIPELINE_ID -gt $PIPELINE ]]; then
            PIPELINE_BRANCH=$(curl -kLs --header "PRIVATE-TOKEN:${ACCESS_TOKEN}" "https://gitlab.bt/api/v4/projects/${CI_PROJECT_ID}/pipelines/$PIPELINE" | jq -r '.ref')
            # 不同分支的Pipeline可以并行
            if [ "$PIPELINE_BRANCH" == "$CI_COMMIT_REF_NAME" ]; then
                # 忽略含有Job:"win64:wait"的流水线
                if [[ "$(curl -kLs --header "PRIVATE-TOKEN:${ACCESS_TOKEN}" "https://gitlab.bt/api/v4/projects/${CI_PROJECT_ID}/pipelines/$PIPELINE/jobs" | jq -r '.[].name')" =~ "win64:wait" ]]; then
                    continue
                fi

                REMOTE_PIPELINE_PLATFORM=$(curl -kLs --header "PRIVATE-TOKEN:${ACCESS_TOKEN}" "https://gitlab.bt/api/v4/projects/${CI_PROJECT_ID}/pipelines/$PIPELINE/variables" | jq -c --arg key "PIPELINE_PLATFORM" '.[] | select(.key == $key)' | jq -r '.value')
                REMOTE_SELECT_MACHINE=$(curl -kLs --header "PRIVATE-TOKEN:${ACCESS_TOKEN}" "https://gitlab.bt/api/v4/projects/${CI_PROJECT_ID}/pipelines/$PIPELINE/variables" | jq -c --arg key "SELECT_MACHINE" '.[] | select(.key == $key)' | jq -r '.value')
                PLATFROM_WAIT_FLAG="false"
                PLATFORM_LIST=(${PIPELINE_PLATFORM//,/ })
                for EACH_PLATFORM in ${PLATFORM_LIST[@]};do
                    if [[ "$REMOTE_PIPELINE_PLATFORM" =~ "$EACH_PLATFORM" ]]; then
                        PLATFROM_WAIT_FLAG="true"
                        break
                    fi
                done
                if [[ "$SELECT_MACHINE" == "$REMOTE_SELECT_MACHINE"  && "$PLATFROM_WAIT_FLAG" == "true" ]]; then
                    WAIT_FLAG="true"
                    echo "未完成流水线：$PIPELINE，等待中......"
                    break
                elif [[ "$REMOTE_SELECT_MACHINE" == "" || "$REMOTE_PIPELINE_PLATFORM" == "" ]]; then
                    WAIT_FLAG="true"
                    echo "未完成流水线：$PIPELINE，等待中......"
                    break
                fi
            fi
        fi
    done
    if [ "$WAIT_FLAG" == "false" ]; then 
        exit 0; 
    else
        # 1分钟判断一次前面是否还有未完成流水线
        sleep 1m
    fi
done
echo $'\x1b[31;1m超过最大等待时间，正在退出...\x1b[0;m'
exit 1
