# coding=utf-8
import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import re
from ci import *

android_sdk = env("ANDROID_SDK")
game_id = env("GAME_ID")
debug_release = "Debug" if env_bool("IS_DEBUG") else "Release"
assemble_tasks = env("ASSEMBLE_TASKS", "") if env("ASSEMBLE_TASKS") != "null" else ""
bundle_id = env("APPLICATION_ID") if env("APPLICATION_ID") else re.search(r'[a-z]*(?:\.[a-z]*)+', find_log(android_sdk + "/build.gradle","applicationId")[0]).group(0)

log("上传包体文件到repo.bt")
# assemble可以为空字符串
for assemble in assemble_tasks.split(","):
    if android_sdk != "sdkone" and android != "msdkone":
        apk_path = android_sdk + "/build/outputs/apk/" + assemble + "/" + debug_release + "/" + android_sdk + "-" + assemble + "-" + debug_release + ".apk"
    else:
        apk_path = android_sdk + "/build/outputs/apk/" + debug_release + "/" + android_sdk + "-" + debug_release + ".apk"
    # 去除可能为空的assemble
    default_name = [x for x in [android_sdk, assemble, debug_release] if x != '']
    default_url = game_id + "/android/" + "_".join(default_name)

    # 添加流水线开始时间
    d = gitlab_api("projects/{}/pipelines/{}".format(env("CI_PROJECT_ID"), env("CI_PIPELINE_ID")))["created_at"]
    pipeline_date = "{}-{}-{}-{}".format(d[5:7], d[8:10], d[11:13], d[14:16])
    sdk_login = "" if env_bool("ENABLE_SDK_LOGIN") else "nosdk"

    backup_name = [x for x in [env("CI_PIPELINE_ID"), android_sdk, assemble, debug_release, env("RESOURCE_VERSION"), env("VERSION_NAME"), env("GAMECONFIG_URL_TYPE"), env("CI_COMMIT_REF_SLUG"), pipeline_date, sdk_login] if x != '']
    backup_url = game_id + "/android/backup/" + "_".join(backup_name)

    upload_nexus(apk_path, backup_url + ".apk")
    inform_apk = "http://repo.bt/repository/" + backup_url + ".apk"
    inform_text = "<font color=\"info\">提醒</font>\n" \
        + "流水线：[{}](https://gitlab.bt/{}/pipelines/{}) 已生成apk包体\n".format(env("CI_PIPELINE_ID"), env("CI_PROJECT_PATH"), env("CI_PIPELINE_ID")) \
        + "下载地址：\n[{}]({})".format(inform_apk, inform_apk)
    if env_bool("SPLIT_OBB"):
        upload_nexus(env("CI_PIPELINE_ID") + ".obb", backup_url + ".obb")
        obb_url = "/android/main." + env("VERSION_CODE") + "." + bundle_id
        upload_nexus(apk_path, obb_url + ".apk")
        upload_nexus(env("CI_PIPELINE_ID") + ".obb", obb_url + ".obb")
        inform_obb = "http://repo.bt/repository/" + backup_url + ".obb"
        inform_text = inform_text + "\n[{}]({})".format(inform_obb, inform_obb)
    else:
        upload_nexus(apk_path, default_url + ".apk")
    if env("CI_PIPELINE_SOURCE") != "schedule":
        inform_weixinwork(inform_text, env("INFORM_KEY"), msgtype = "markdown")
        inform_weixinwork("", env("INFORM_KEY"), at = env("INFORM_USER", env("GITLAB_USER_LOGIN")))

    write_file("../package-url-android.txt", inform_apk)
