# coding=utf-8
import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from ci import *

bugly_dir = "./temp"
if "retry-bugly" not in env("CI_JOB_NAME"):
    cp("./libgame/src/main/jniLibs", bugly_dir)
    if not env_bool("IS_MONO_BUILD"):
        for file in find(bugly_dir, kind = "relative", type_filter = "file"):
            if "libunity" in file or "libil2cpp" in file:
                rm(file)
        for arch in ls("./StagingArea/symbols"):
            mkdir(os.path.join(bugly_dir, arch))
            cp("./StagingArea/symbols/{}/*".format(arch), os.path.join(bugly_dir, arch))

    for file in find("libgame", kind = "relative", type_filter = "file"):
        if file.endswith(".debug"):
            mv(file, file[:-6])

    if not env_bool("IS_DEBUG") and env("CI_PIPELINE_SOURCE") != "schedule":
        mkdir(ci_home("bugly-symbol", env("CI_PROJECT_PATH")))
        rm(ci_home("bugly-symbol", env("CI_PROJECT_PATH"), env("PLATFORM") + env("CI_COMMIT_REF_SLUG")))
        cp(bugly_dir, ci_home("bugly-symbol", env("CI_PROJECT_PATH"), env("PLATFORM") + env("CI_COMMIT_REF_SLUG")))
elif "retry-bugly" in env("CI_JOB_NAME"):
    rm(bugly_dir)
    cp(ci_home("bugly-symbol", env("CI_PROJECT_PATH"), env("PLATFORM") + env("CI_COMMIT_REF_SLUG")), bugly_dir)

bugly_env = {
    "sdkone": ["BUGLY_APP_ID_ANDROID","BUGLY_APP_KEY_ANDROID"],
    "msdkone": ["BUGLY_APP_ID_ANDROID","MSDK_BUGLY_APP_KEY_ANDROID"],
    "ubeejoy": ["UBJ_BUGLY_APP_ID_ANDROID","UBJ_BUGLY_APP_KEY_ANDROID"],
    "ubeejoy_kr": ["UBJ_KR_GP_BUGLY_APP_ID_ANDROID","UBJ_KR_GP_BUGLY_APP_KEY_ANDROID",
        "UBJ_KR_ONESTORE_BUGLY_APP_ID_ANDROID","UBJ_KR_ONESTORE_BUGLY_APP_KEY_ANDROID"],
    "efun": ["EFUN_BUGLY_APP_ID_ANDROID","EFUN_BUGLY_APP_KEY_ANDROID"],
    "efun_jp": ["EFUN_JP_BUGLY_APP_ID_ANDROID","EFUN_JP_BUGLY_APP_KEY_ANDROID"],
    "youzu_kr": ["YOUZU_KR_BUGLY_APP_ID_ANDROID","YOUZU_KR_BUGLY_APP_KEY_ANDROID"]
}

android_sdk = env("ANDROID_SDK")
bundle_id = env("APPLICATION_ID") if env("APPLICATION_ID") else re.search(r'[a-z]*(?:\.[a-z]*)+', find_log(android_sdk + "/build.gradle","applicationId")[0]).group(0)
if env("ASSEMBLE_TASKS", "").lower() in ["onestorekr"]:
    bugly_id = env(bugly_env[android_sdk][2])
    bugly_key = env(bugly_env[android_sdk][3])
else:
    bugly_id = env(bugly_env[android_sdk][0])
    bugly_key = env(bugly_env[android_sdk][1])
upload_bugly(bugly_dir, "Android", bugly_id, bugly_key, env("VERSION_NAME"), bundle_id)
