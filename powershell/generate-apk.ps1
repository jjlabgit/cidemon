$ErrorActionPreference = "Stop";

# 删除目录下不要的多语言文件
if (${BUILD_LANG} -ne "all") {
    $LANG_DIR="./libgame/src/main/assets/Android/language";
    (ls $LANG_DIR).Name | ForEach-Object {
        if (!(${BUILD_LANG} -match "(^|,)$_($|,)") -and (Test-Path ${LANG_DIR}/$_)) {
            rm -r -Force ${LANG_DIR}/$_;
        }
    }
}

$env:ORG_GRADLE_PROJECT_btObbSize="X_0";
$BUILD_ID = (Select-String ./libgame/src/main/AndroidManifest.xml -Pattern "unity.build-id").Line.Split('"')[3];
Write-Host "[unity.build-id]: ${BUILD_ID}" -fore green;
# 是否打obb
if (${SPLIT_OBB} -eq "true") {
    Write-Host "拆分OBB资源文件" -fore green;
    if (Test-Path "./${ANDROID_SDK}/obb-keep-list") {
        $OBB_KEEP_LIST="./${ANDROID_SDK}/obb-keep-list";
    } elseif (Test-Path "./obb-keep-list") {
        $OBB_KEEP_LIST="./obb-keep-list";
    } else {
        Write-Host "配置文件obb-keep-list不存在！正在退出..." -fore red;
        exit 1;
    }
    New-Item ./assets -ItemType Directory;

    # 区分是否是海外包 和android一样构建obb 
    if (${ANDROID_SDK} -eq "ubeejoy") {
        mv -Force ./libgame/src/main/assets/Android ./assets/Android;
        mv -Force ./libgame/src/main/assets/gamevideo ./assets/gamevideo;
        mv -Force ./libgame/src/main/assets/gameaudio ./assets/gameaudio;

        (cat ${OBB_KEEP_LIST}) | ForEach-Object {
            if (!(Test-Path ./assets/$_)) {
                echo "警告：obb-keep-list中配置的AndroidProj/libgame/src/main/assets/$_文件或文件夹不存在！" -fore red;
            } else {
                <# 下面两步先New-Item再rm相当于mkdir -p母目录，是没有ditto命令的替代做法 #>
                New-Item ./libgame/src/main/assets/$_ -ItemType Directory;
                rm -r -Force ./libgame/src/main/assets/$_;
                mv -Force ./assets/$_ ./libgame/src/main/assets/$_;
            }
        }
		python ../CITools/split-obb-ubeejoy.py assets "assets/${BUILD_ID}";
		if ($LASTEXITCODE -ne 0) {exit 1;}
    } else {
        python ../CITools/small-package-pick.py libgame/src/main/assets/Android assets/Android;
        if ($LASTEXITCODE -ne 0) {exit 1;}
    }

    # 根据 obb配置或者拷贝出来的资源，压缩成obb文件
    if ((${ANDROID_SDK} -eq "ubeejoy") -and (${FORCE_OLD_OBB} -ne "false")) {
        $BUILD_ID = ${OLD_OBB_BUILD_ID};
        Write-Host "old [unity.build-id]: ${BUILD_ID}" -fore green;
        (Get-Content ./libgame/src/main/AndroidManifest.xml) -replace '[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}', ${OLD_OBB_BUILD_ID} | Out-File ./libgame/src/main/AndroidManifest.xml
        $env:ORG_GRADLE_PROJECT_btObbSize = ${OLD_OBB_SIZE};
        $env:ORG_GRADLE_PROJECT_btObbVersion = ${OLD_OBB_VERSION};

    } else {
        New-Item ./assets/${BUILD_ID} -ItemType File;
		if (${ANDROID_SDK} -eq "ubeejoy"){
			& 'C:\Program Files\7-Zip\7z.exe' a -tzip "${CI_PIPELINE_ID}.obb" -i@"./main_obb.txt";
			if ($LASTEXITCODE -ne 0) {exit 1;}
			$env:ORG_GRADLE_PROJECT_btObbSize = "X_" + (ls ./${CI_PIPELINE_ID}.obb).Length;
			$env:ORG_GRADLE_PROJECT_btObbVersion = ${VERSION_CODE};
			
			if (Test-Path "./patch_obb.txt"){
				& 'C:\Program Files\7-Zip\7z.exe' a -tzip "patch_${CI_PIPELINE_ID}.obb" -i@"./patch_obb.txt";
				if ($LASTEXITCODE -ne 0) {exit 1;}
				$env:ORG_GRADLE_PROJECT_btObbPatchSize = "X_" + (ls ./patch_${CI_PIPELINE_ID}.obb).Length;
			}
		}else{
			& 'C:\Program Files\7-Zip\7z.exe' a -tzip "${CI_PIPELINE_ID}.obb" ./assets;
			if ($LASTEXITCODE -ne 0) {exit 1;}
			$env:ORG_GRADLE_PROJECT_btObbSize = "X_" + (ls ./${CI_PIPELINE_ID}.obb).Length;
			$env:ORG_GRADLE_PROJECT_btObbVersion = ${VERSION_CODE};
		}
    }

}

# 区分包是否是debug 或者正式包
<# 出包 #>
Write-Host "出包" -fore green;
$env:GRADLE_OPTS="-Dorg.gradle.daemon=false " + $env:GRADLE_OPTS
if (${IS_DEBUG} -eq "true") {
    $DEBUG_RELEASE="Debug";
} else {
    $DEBUG_RELEASE="Release";
}
if (${ASSEMBLE_TASK} -ne "null") {
    ./gradlew.bat clean :${ANDROID_SDK}:${ASSEMBLE_TASK} --stacktrace -g "C:/gitlab-runner/gradle-cache";
} else {
    ./gradlew.bat clean :${ANDROID_SDK}:assemble${DEBUG_RELEASE} --stacktrace -g "C:/gitlab-runner/gradle-cache";
}
if ($LASTEXITCODE -ne 0) {exit 1;}

<# 将libgame/src/main/assets/Android目录复原 #>
if (${SPLIT_OBB} -eq "true") {
    if (!(Test-Path ./libgame/src/main/assets/Android)) { New-Item ./libgame/src/main/assets/Android -ItemType Directory; }
    cp -R -Force ./assets/Android/* ./libgame/src/main/assets/Android;
    if (${ANDROID_SDK} -eq "ubeejoy") {
        if (!(Test-Path ./libgame/src/main/assets/gamevideo)) { New-Item ./libgame/src/main/assets/gamevideo -ItemType Directory; }
		cp -R -Force ./assets/gamevideo/* ./libgame/src/main/assets/gamevideo;
        if (!(Test-Path ./libgame/src/main/assets/gameaudio)) { New-Item ./libgame/src/main/assets/gameaudio -ItemType Directory; }
		cp -R -Force ./assets/gameaudio/* ./libgame/src/main/assets/gameaudio;
    }
}
