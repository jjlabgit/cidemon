# coding=utf-8
from requests.utils import quote
import filecmp
import time
import os
import signal
import psutil
from ci import *

def wait_for_finishing():
    max_wait_min = 300
    query_interval_sec = 60
    finish_flag = False

    for i in range(int(60 / query_interval_sec * max_wait_min)):
        if find_log("log.txt", "EditorScheduleMgr All tasks End!"):
            log("检测到结束日志'EditorScheduleMgr All tasks End!'，正在退出Unity...")
            finish_flag = True
            break
        else:
            time.sleep(query_interval_sec)
    if not finish_flag:
        log("超过最大等待时间，正在退出...", "red")
        return False

    error_logs = [
        # 此处不能输入中文
        "Example: text your error log here",
    ]
    for error_log in error_logs:
        if find_log("log.txt", error_log):
            log("构建出错: " + error_log, "red")
            return False
    return True


if __name__ == '__main__':
    file_path = "UnityProj/Assets/GameAssets/shared/urpshaders/customshadervariants.shadervariants"
    yaml_path = "UnityProj/Assets/GameAssets/shared/urpshaders/customshadervariants_format.yaml"
    old_file = "customshadervariants.shadervariants.old"
    old_yaml = "customshadervariants_format.yaml.old"
    if os.path.exists(file_path):
        cp(file_path, old_file)
    if os.path.exists(yaml_path):
        cp(yaml_path, old_yaml)

    buildTarget, platformIndex = getUnityPlatformInfo(env("PLATFORM"))
    unity(buildTarget = buildTarget, logFile = "import.txt")

    for cache in ["UnityProj/Library/ShaderCache", "UnityProj/Library/ShaderCache.db"]:
        if os.path.exists(cache):
            log("正在删除：" + cache)
            rm(cache)

    process = unity(
        blocking = False,
        batchmode = "false",
        quit = "false",
        executeMethod = "Astral.Tools.CollectionShaderVariantUtility.CollectionMaterial"
    )

    try:
        is_success = wait_for_finishing()
    finally:
        for proc in psutil.process_iter():
            if proc.ppid() == process.pid and proc.name() == "Unity":
                os.kill(proc.pid, signal.SIGTERM)
    if not is_success:
        exit(1)

    error_logs = [
        # 此处不能输入中文
        "Failed executing selected SpritePackerPolicy.ArgumentException",
        "Receiving unhandled NULL exception",
        "EditorApplicationIsCompiling10086",
        "Failed to compile player scripts",
        "Build Task WriteSerializedFiles failed with exception",
        "error! Subprogram failed",
        "=>Object reference not set to an instance of an object",
        "ArgumentOutOfRangeException: Index was out of range."
    ]
    for error_log in error_logs:
        if find_log("log.txt", error_log):
            error("构建出错: " + error_log)

    if not grep(cat(yaml_path, stdout = False, mode = "list"), "UI/Default"):
        error("customshadervariants_format.yaml中缺少UI/Default，采集失败。原因暂时未知，先报错处理。")

    actions = []
    if not os.path.exists(old_file):
        actions.append({"action": "create", "file_path": file_path, "content": cat(file_path, stdout = False)})
    elif not filecmp.cmp(file_path, old_file):
        actions.append({"action": "update", "file_path": file_path, "content": cat(file_path, stdout = False)})
    if not os.path.exists(old_yaml):
        actions.append({"action": "create", "file_path": yaml_path, "content": cat(yaml_path, stdout = False)})
    elif not filecmp.cmp(yaml_path, old_yaml):
        actions.append({"action": "update", "file_path": yaml_path, "content": cat(yaml_path, stdout = False)})

    if actions:
        data = {
            "branch": env("CI_COMMIT_REF_NAME"),
            "commit_message": "[ci skip]CI流水线{}自动提交变体采集".format(env("CI_PIPELINE_ID")),
            "author_email": env("GITLAB_USER_EMAIL"),
            "author_name": env("GITLAB_USER_LOGIN"),
            "actions": actions
        }
        result = gitlab_api("projects/{}/repository/commits".format(env("CI_PROJECT_ID")), method = "post", headers = {"Content-Type": "application/json"}, data = json.dumps(data))
        if type(result) == str or "id" not in result:
            error("API提交采集变体失败！输出为：" + str(result))
        else:
            log("采集变体已提交到Commit:{}，链接为： {}".format(result["short_id"], result["web_url"]))
    else:
        log("变体采集后customshadervariants.shadervariants文件和customshadervariants_format.yaml皆无变化！不需要提交到git")

    cp(file_path, "customshadervariants.shadervariants")
    cp(yaml_path, "customshadervariants_format.yaml")
