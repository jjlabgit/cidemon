# coding=utf-8
from filecmp import dircmp
import os
import sys
import shutil
import distutils
from distutils import dir_util
import xml.etree.ElementTree as ET
from ci import *


# 简单的XML文件美化
def indent(elem, level=0):
    i = os.linesep + level*"  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            indent(elem, level+1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i


def create_xml(dcmp, out, root_node, hotupdate_base, ab_dir, ignore = []):
    for name in dcmp.left_only + dcmp.right_only + dcmp.diff_files:
        if name in dcmp.left_only:
            src_path = os.path.join(dcmp.left, name)
            xml_opt = "delete"
            path_prefix = hotupdate_base.replace("\\", "/") + "/"
        elif name in dcmp.right_only:
            src_path = os.path.join(dcmp.right, name)
            xml_opt = "add"
            path_prefix = ab_dir.replace("\\", "/") + "/"
        else:
            src_path = os.path.join(dcmp.right, name)
            xml_opt = "update"
            path_prefix = ab_dir.replace("\\", "/") + "/"
        if os.path.isdir(src_path):
            for top, dirs, files in os.walk(src_path):
                for f in files:
                    file_path = os.path.join(top, f)
                    path_slash = file_path.replace("\\", "/").replace(path_prefix, "")
                    ignore_flag = False
                    for element in ignore:
                        if path_slash == element or path_slash[:len(element) + 1] == element + "/":
                            ignore_flag = True
                            break
                    if not ignore_flag:
                        ET.SubElement(root_node, "file",
                                      path=path_slash,
                                      opt=xml_opt,
                                      size=str(os.path.getsize(file_path)))
                        log(xml_opt + " file " + path_slash, "white")
        else:
            path_slash = src_path.replace("\\", "/").replace(path_prefix, "")
            ignore_flag = False
            for element in ignore:
                if path_slash == element or path_slash[:len(element) + 1] == element + "/":
                    ignore_flag = True
                    break
            if not ignore_flag:
                ET.SubElement(root_node, "file",
                              path=path_slash,
                              opt=xml_opt,
                              size=str(os.path.getsize(src_path)))
                log(xml_opt + " file " + path_slash, "white")
    for sub_dcmp in dcmp.subdirs.values():
        create_xml(sub_dcmp, out, root_node, hotupdate_base, ab_dir, ignore = ignore)


def copy_diff_files(dcmp, out):
    for name in dcmp.right_only + dcmp.diff_files:
        src_path = os.path.join(dcmp.right, name)
        dist_dir = os.path.join(out, dcmp.right)
        if os.path.isdir(src_path):
            dist_dir = os.path.join(out, dcmp.right, name)
            distutils.dir_util.copy_tree(src_path, dist_dir)
        else:
            if not os.path.isdir(dist_dir):
                os.makedirs(dist_dir)
            shutil.copy(src_path, dist_dir)
    for sub_dcmp in dcmp.subdirs.values():
        copy_diff_files(sub_dcmp, out)

if __name__ == '__main__':
    platform = env("PLATFORM")
    if platform == "android":
        platform_dir = "aa/Android/Android"
    elif platform == "ios":
        platform_dir = "aa/iOS/iOS"
    else:
        platform_dir = "aa/Windows/StandaloneWindows64"
    ab_dir = os.path.join("UnityProj/Library/com.unity.addressables", platform_dir)
    hotupdate_base = ci_home("hotupdate", env("CI_PROJECT_PATH"), env("CI_COMMIT_REF_SLUG"), platform)
    hotupdate_base_ab = os.path.join(hotupdate_base, platform_dir)

    if not os.path.exists(hotupdate_base):
        log("未发现前一次构建缓存")
        mkdir(hotupdate_base)
        cp("UnityProj/Library/com.unity.addressables/*", hotupdate_base)
        exit(0)

    out_dir = "./hotupdate-output"
    # ignore不能包含空字符串
    ignore = [x for x in env("HOTUPDATE_IGNORES","").split(",") if x != '']

    # 删除其他语言的AB资源
    lang_dir = ab_dir + "/language_assets_language"
    build_lang = env("BUILD_LANG", "all")
    if os.path.isdir(lang_dir) and build_lang != "all":
        for language in ls(lang_dir):
            if language not in build_lang.split(","):
                ignore.append("language_assets_language/" + language)

    if os.path.isdir(hotupdate_base_ab) and os.path.isdir(ab_dir):
        obj = dircmp(hotupdate_base_ab, ab_dir)
        if not os.path.isdir(out_dir):
            os.mkdir(out_dir)
        copy_diff_files(obj, out_dir)
        root = ET.Element("root")
        create_xml(obj, out_dir, root, hotupdate_base_ab, ab_dir, ignore = ignore)
        tree = ET.ElementTree(root)
        indent(root)
        tree.write(os.path.join(out_dir, "filemanifest.xml"), encoding="utf-8", xml_declaration=True)
    else:
        error('params is not a dir')

    diff_dir = os.path.join(out_dir, ab_dir)
    if not os.path.isdir(diff_dir):
        log("没有需要热更的文件", "red")
        rm(out_dir)
        exit(0)

    mv(os.path.join(out_dir, "UnityProj/Library/com.unity.addressables", platform_dir) + "/*", out_dir)
    rm(os.path.join(out_dir, "UnityProj"))

    for useless in ignore:
        rm(os.path.join(out_dir, useless))

    # 删除空文件夹
    while True:
        empty_flag = False
        for root, dirs, files in os.walk(out_dir):
            if not os.listdir(root):
                empty_flag = True
                os.rmdir(root)
        if not empty_flag:
            break

    inform_text = "<font color=\"info\">提醒</font>\n" \
        + "流水线：[{}](https://gitlab.bt/{}/pipelines/{}) 已生成热更包\n".format(env("CI_PIPELINE_ID"), env("CI_PROJECT_PATH"), env("CI_PIPELINE_ID")) \
        + "下载地址：[https://gitlab.bt/{}/-/jobs/{}/artifacts/download](https://gitlab.bt/{}/-/jobs/{}/artifacts/download)".format(env("CI_PROJECT_PATH"), env("CI_JOB_ID"),env("CI_PROJECT_PATH"), env("CI_JOB_ID"))
    if env("CI_PIPELINE_SOURCE") != "schedule":
        inform_weixinwork(inform_text, env("INFORM_KEY"), msgtype = "markdown")
        inform_weixinwork("", env("INFORM_KEY"), at = env("INFORM_USER", env("GITLAB_USER_LOGIN")))
