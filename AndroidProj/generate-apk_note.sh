#!/bin/sh
set -e

# 删除其他语言的AB资源
if [[ "${BUILD_LANG}" != "all" ]]; then
    LANG_DIR=./libgame/src/main/assets/Android/language
    for LANGUAGE in $(ls ${LANG_DIR}); do
        if [[ ! ${BUILD_LANG} =~ (^|,)${LANGUAGE}($|,) ]];then
            rm -rf ${LANG_DIR}/${LANGUAGE}
        fi
    done
fi

# 拆分libgame/src/main/assets/Android下要打进obb包的AB资源
# obb-keep-list存放在"AndroidProj/${ANDROID_SDK}/"或"AndroidProj/"(缺省)目录下，文件中定义的文件或文件夹打进apk包，其他则打进obb包
# obb-keep-list不支持通配符
export ORG_GRADLE_PROJECT_btObbSize=0
BUILD_ID=$(grep unity.build-id ./libgame/src/main/AndroidManifest.xml | grep -o "[0-9a-fA-F]\{8\}-[0-9a-fA-F]\{4\}-[0-9a-fA-F]\{4\}-[0-9a-fA-F]\{4\}-[0-9a-fA-F]\{12\}") || true
echo "[unity.build-id]: ${BUILD_ID}"
# 先判断是否构建obb
if [[ "${SPLIT_OBB}" == "true" ]]; then
    echo $"\x1b[32;1m拆分OBB资源文件\x1b[0;m"
    if [ -f "./${ANDROID_SDK}/obb-keep-list" ]; then
        OBB_KEEP_LIST=./${ANDROID_SDK}/obb-keep-list
    elif [ -f "./obb-keep-list" ]; then
        OBB_KEEP_LIST=./obb-keep-list
    else
        echo $'\x1b[31;1m配置文件obb-keep-list不存在！正在退出...\x1b[0;m'
        exit 1
    fi 
	
    mkdir ./assets
    # 区分是否是海外渠道
    if [[ "${ANDROID_SDK}" == "ubeejoy" ]]; then
        # 把打出的资源全部从目录中拷贝出去 (海外打的是小包，包内不留资源，资源打成多个obb)
        mv -f ./libgame/src/main/assets/Android ./assets/Android
        mv -f ./libgame/src/main/assets/gamevideo ./assets/gamevideo
        mv -f ./libgame/src/main/assets/gameaudio ./assets/gameaudio
        for DATA in $(cat obb-keep-list); do
            if [ ! -e "./assets/${DATA}" ]; then
                echo $"\x1b[31;1m警告：obb-keep-list中配置的AndroidProj/libgame/src/main/assets/${DATA}文件或文件夹不存在！\x1b[0;m"
            else
                ditto ./assets/${DATA} ./libgame/src/main/assets/${DATA}
                rm -rf ./assets/${DATA}
            fi
        done
		# 构建obb包操作(打多个obb,这里实际都只是把文件写入到一个文本中记录起来)
		python ../CITools/split-obb-ubeejoy.py assets "assets/${BUILD_ID}"
    else
        # 国内obb 构建 (获取设置的obb资源大小，拷贝出去打成obb包,目前只打一个，随着包体越来越大也是有可能打多个的)
        python ../CITools/small-package-pick.py libgame/src/main/assets/Android assets/Android
    fi

    if [[ "${ANDROID_SDK}" == "ubeejoy" && "${FORCE_OLD_OBB}" != "false" ]]; then
        BUILD_ID="${OLD_OBB_BUILD_ID}"
        echo "old [unity.build-id]: ${BUILD_ID}"
        sed -i "" '/unity.build-id/s/value="[[0-9a-fA-F]\{8\}-[0-9a-fA-F]\{4\}-[0-9a-fA-F]\{4\}-[0-9a-fA-F]\{4\}-[0-9a-fA-F]\{12\}"/value="'"${OLD_OBB_BUILD_ID}"'"/' ./libgame/src/main/AndroidManifest.xml
        export ORG_GRADLE_PROJECT_btObbSize=${OLD_OBB_SIZE}
        export ORG_GRADLE_PROJECT_btObbVersion=${OLD_OBB_VERSION}
    else
        touch ./assets/${BUILD_ID}
        # 实际obb操作
		if [[ "${ANDROID_SDK}" == "ubeejoy" ]]; then
            # 海外是将记录obb的文件 设置到zip的压缩命令中，然后执行压缩操作
			zip -r ${CI_PIPELINE_ID}.obb -@ < main_obb.txt
			SIZE_TEMP=$(wc -c ./${CI_PIPELINE_ID}.obb)
			export ORG_GRADLE_PROJECT_btObbSize=X_$(echo ${SIZE_TEMP% *})
			export ORG_GRADLE_PROJECT_btObbVersion=${VERSION_CODE}
			if [[ -f "./patch_obb.txt" ]]; then
				zip -r patch_${CI_PIPELINE_ID}.obb -@ < patch_obb.txt
				SIZE_TEMP=$(wc -c ./patch_${CI_PIPELINE_ID}.obb)
				export ORG_GRADLE_PROJECT_btObbPatchSize=X_$(echo ${SIZE_TEMP% *})
			fi	
		else
            # 国内 是将上面拷贝出去的资源，全部压缩到obb中
			zip -r ${CI_PIPELINE_ID}.obb ./assets
			SIZE_TEMP=$(wc -c ./${CI_PIPELINE_ID}.obb)
			export ORG_GRADLE_PROJECT_btObbSize=X_$(echo ${SIZE_TEMP% *})
			export ORG_GRADLE_PROJECT_btObbVersion=${VERSION_CODE}
		fi     
    fi
fi

# 出包
echo $"\x1b[32;1m出包\x1b[0;m"
# 为./gradlew添加可执行权限

# 判断包体是否debug包
chmod +x ./gradlew
if [[ "${IS_DEBUG}" == "true" ]]; then
    DEBUG_RELEASE=Debug
else
    DEBUG_RELEASE=Release
fi

if [[ "${ASSEMBLE_TASK}" != "null" ]]; then
    ./gradlew clean :${ANDROID_SDK}:${ASSEMBLE_TASK} --stacktrace
else
    ./gradlew clean :${ANDROID_SDK}:assemble${DEBUG_RELEASE} --stacktrace
fi

# 将libgame/src/main/assets/Android目录复原
if [[ "${SPLIT_OBB}" == "true" ]]; then
    ditto ./assets/Android ./libgame/src/main/assets/Android
    if [[ "${ANDROID_SDK}" == "ubeejoy" ]]; then
		ditto ./assets/gamevideo ./libgame/src/main/assets/gamevideo
		ditto ./assets/gameaudio ./libgame/src/main/assets/gameaudio
    fi
fi
