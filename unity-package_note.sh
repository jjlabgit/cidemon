#!/bin/sh
set -e
#执行unity 调用项目内c#打包逻辑
RESULT=0
if [[ "${PLATFORM}" == "Android" ]]; then
    ${UNITY_EXE_PATH} \
        -batchmode \
        -projectPath ${CI_PROJECT_DIR}/UnityProj \
        -buildTarget ${BUILD_TARGET} \
        -executeMethod ${BUILD_PACKAGE_METHOD} \
        -gameConfigUrlType ${GAMECONFIG_URL_TYPE} \
        -versionName ${VERSION_NAME} \
        -versionCode ${VERSION_CODE} \
        -isDebug ${IS_DEBUG} \
        -isProfiler ${IS_DEBUG} \
        -branchName ${CI_COMMIT_REF_SLUG} \
        -quit \
        -logFile | tee ./log.txt || RESULT=${PIPESTATUS[0]}
elif [[ "${PLATFORM}" == "IOS" ]]; then
    ${UNITY_EXE_PATH} \
        -batchmode \
        -projectPath ${CI_PROJECT_DIR}/UnityProj \
        -buildTarget ${BUILD_TARGET} \
        -executeMethod ${BUILD_PACKAGE_METHOD} \
        -gameConfigUrlType ${GAMECONFIG_URL_TYPE} \
        -versionName ${VERSION_NAME} \
        -versionCode ${VERSION_CODE} \
        -isDebug ${IS_DEBUG} \
        -isProfiler ${IS_DEBUG} \
        -branchName ${CI_COMMIT_REF_SLUG} \
        -quit \
        -logFile | tee ./log.txt || RESULT=${PIPESTATUS[0]}
fi

echo "The return code of Unity is $RESULT"
KEY_LOG=$(cat ./log.txt | grep "IOException: Disk full") || true
if [[ "" != "$KEY_LOG" ]]; then
    echo $'\x1b[31;1m构建出错: 磁盘空间不足（IOException: Disk full）,请清理不需要的文件\x1b[0;m'
    exit 2
fi
KEY_LOG=$(cat ./log.txt | grep "Unable to locate Android") || true
if [[ "" != "$KEY_LOG" ]]; then
    echo $'\x1b[31;1m构建出错: 找不到Android SDK或NDK，请在构造机上配置环境变量并将其添加到PATH\x1b[0;m'
    exit 3
fi
KEY_LOG=$(cat ./log.txt | grep "Aborting batchmode due to failure") || true
if [[ "" != "$KEY_LOG" ]]; then
    echo $'\x1b[31;1m构建出错: Aborting batchmode due to failure\x1b[0;m'
    exit 4
fi
if [[ "$RESULT" -ne 0 ]]; then
    echo $'\x1b[31;1m构建出错，详情请查询日志\x1b[0;m'
    exit $RESULT
fi
