# coding=utf-8
import os
import sys
from ci import *

if __name__ == '__main__':
    # 如果分支保存基线时提交不是最新的，需要重新强打生成AB
    commit_flag = False
    pipelines = gitlab_api("projects/{}/pipelines?ref={}".format(env("CI_PROJECT_ID"), env("CI_COMMIT_REF_NAME")))
    for pipeline in pipelines:
        if int(env("CI_PIPELINE_ID")) < int(pipeline["id"]):
            sha = gitlab_api("projects/{}/pipelines/{}".format(env("CI_PROJECT_ID"), pipeline["id"]))['sha']
            variables = gitlab_api("projects/{}/pipelines/{}/variables".format(env("CI_PROJECT_ID"), pipeline["id"]))
            if sha == env("CI_COMMIT_SHA"):
                break
            else:
                remote_machine = env("SELECT_MACHINE")
                for var in variables:
                    if var["key"] == "SELECT_MACHINE":
                        remote_machine = var["value"]
            if remote_machine == env("SELECT_MACHINE"):
                jobs = gitlab_api("projects/{}/pipelines/{}/jobs".format(env("CI_PROJECT_ID"), pipeline["id"]))
                for job in jobs:
                    if job["name"].split(":")[0] == env("PLATFORM") and job["status"] not in ["skipped", "manual"]:
                        commit_flag = True
                        break
    if commit_flag:
        log("当前流水线的Commit已经不是最新流水线的Commit，因此需要重新强打当前Commit的AB！")
        set_env("CLEAN_CACHE", "true")
        shell("python unity-bundle.py")



    platform = env("PLATFORM")
    if platform == "android":
        platform_dir = "aa/Android/Android"
    elif platform == "ios":
        platform_dir = "aa/iOS/iOS"
    else:
        platform_dir = "aa/Windows/StandaloneWindows64"
    ab_dir = os.path.join("UnityProj/Library/com.unity.addressables", platform_dir)
    hotupdate_base = ci_home("hotupdate", env("CI_PROJECT_PATH"), env("CI_COMMIT_REF_SLUG"), platform)

    zip_py("update-baseline.zip", ab_dir)
    upload_nexus("update-baseline.zip", env("GAME_ID") + "/backup/hotupdate-base/" + env("PLATFORM") + "-" + env("CI_COMMIT_REF_SLUG") + ".zip")
    
    rm(hotupdate_base + "-lasttime")
    if os.path.isdir(hotupdate_base):
        mv(hotupdate_base, hotupdate_base + "-lasttime")
        mkdir(hotupdate_base)
        cp("UnityProj/Library/com.unity.addressables/*", hotupdate_base)
