# coding=utf-8
# pylint: disable=print-statement
from filecmp import dircmp
import os
import sys
import shutil
import distutils
from distutils import dir_util
import xml.etree.ElementTree as ET

# 对比ab差异，把有差异的ab写入到文件中，作为热更资源
# 简单的XML文件美化
def indent(elem, level=0):
    i = os.linesep + level*"  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            indent(elem, level+1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i


def create_xml(dcmp, out, root_node):
    for name in dcmp.left_only + dcmp.right_only + dcmp.diff_files:
        if name in dcmp.left_only:
            src_path = os.path.join(dcmp.left, name)
            xml_opt = "delete"
            path_prefix = sys.argv[1]
        elif name in dcmp.right_only:
            src_path = os.path.join(dcmp.right, name)
            xml_opt = "add"
            path_prefix = sys.argv[2]
        else:
            src_path = os.path.join(dcmp.right, name)
            xml_opt = "update"
            path_prefix = sys.argv[2]
        if os.path.isdir(src_path):
            for top, dirs, files in os.walk(src_path):
                for f in files:
                    file_path = os.path.join(top, f)
                    if os.path.splitext(file_path)[1] == ".manifest":
                        if os.path.splitext(f)[0] in files:
                            continue
                        print "%s file %s" % (xml_opt, file_path.replace(path_prefix, ""))
                        ET.SubElement(root_node, "file",
                                      path=os.path.splitext(file_path)[0].replace(path_prefix, ""),
                                      opt=xml_opt,
                                      size=str(os.path.getsize(os.path.splitext(file_path)[0])))
                        continue
                    ET.SubElement(root_node, "file",
                                  path=file_path.replace(path_prefix, ""),
                                  opt=xml_opt,
                                  size=str(os.path.getsize(file_path)))
                    print "%s file %s" % (xml_opt, file_path.replace(path_prefix, ""))
        else:
            if os.path.splitext(src_path)[1] == ".manifest":
                if os.path.splitext(name)[0] in dcmp.left_only + dcmp.right_only + dcmp.diff_files:
                    continue
                print "%s file %s" % (xml_opt, src_path.replace(path_prefix, ""))
                ET.SubElement(root_node, "file",
                              path=os.path.splitext(src_path)[0].replace(path_prefix, ""),
                              opt=xml_opt,
                              size=str(os.path.getsize(os.path.splitext(src_path)[0])))
                continue
            ET.SubElement(root_node, "file",
                          path=src_path.replace(path_prefix, ""),
                          opt=xml_opt,
                          size=str(os.path.getsize(src_path)))
            print "%s file %s" % (xml_opt, src_path.replace(path_prefix, ""))
    for sub_dcmp in dcmp.subdirs.values():
        create_xml(sub_dcmp, out, root_node)


def copy_diff_files(dcmp, out):
    for name in dcmp.right_only + dcmp.diff_files:
        src_path = os.path.join(dcmp.right, name)
        dist_dir = os.path.join(out, dcmp.right)
        if os.path.isdir(src_path):
            dist_dir = os.path.join(out, dcmp.right, name)
            distutils.dir_util.copy_tree(src_path, dist_dir)
        else:
            if not os.path.isdir(dist_dir):
                os.makedirs(dist_dir)
            if os.path.splitext(src_path)[1] == ".manifest":
                shutil.copy(os.path.splitext(src_path)[0], dist_dir)
            shutil.copy(src_path, dist_dir)
    for sub_dcmp in dcmp.subdirs.values():
        copy_diff_files(sub_dcmp, out)


if __name__ == "__main__":
    base_dir = sys.argv[1]
    target_dir = sys.argv[2]
    out_dir = sys.argv[3]
    if os.path.isdir(base_dir) and os.path.isdir(target_dir):
        obj = dircmp(base_dir, target_dir)
        if not os.path.isdir(out_dir):
            os.mkdir(out_dir)
        copy_diff_files(obj, out_dir)
        root = ET.Element("root")
        create_xml(obj, out_dir, root)
        tree = ET.ElementTree(root)
        indent(root)
        tree.write(os.path.join(out_dir, "filemanifest.xml"), encoding="utf-8", xml_declaration=True)
    else:
        print 'params is not a dir'
        exit(1)
