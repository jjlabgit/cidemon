# coding=utf-8
import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from ci import *

log("开始上传IPA包")
PlistBuddy("./ipaout/output/app.xcarchive/Info.plist", "Print :ApplicationProperties:CFBundleIdentifier", stdout = "ci_bundle_id.txt")
bundle_id = cat("ci_bundle_id.txt", stdout = False).replace("\n","")
ios_sdk = env("IOS_SDK")
is_debug = env_bool("IS_DEBUG")
is_distribution = env("IS_DISTRIBUTION")
game_id = env("GAME_ID")

if is_debug and is_distribution == "adhoc":
    archive = "adhoc-debug"
elif not is_debug and is_distribution == "adhoc":
    archive = "adhoc-release"
elif is_debug and convert_bool(is_distribution):
    archive = "distribution-debug"
elif is_debug and not convert_bool(is_distribution):
    archive = "enterprise-debug"
elif not is_debug and convert_bool(is_distribution):
    archive = "distribution-release"
elif not is_debug and not convert_bool(is_distribution):
    archive = "enterprise-release"

# 添加流水线开始时间
d = gitlab_api("projects/{}/pipelines/{}".format(env("CI_PROJECT_ID"), env("CI_PIPELINE_ID")))["created_at"]
pipeline_date = "{}-{}-{}-{}".format(d[5:7], d[8:10], d[11:13], d[14:16])
sdk_login = "" if env_bool("ENABLE_SDK_LOGIN") else "nosdk"

manifestURL = game_id + "/ios/" + "_".join([archive, ios_sdk]) + ".plist"
uploadURL = game_id + "/ios/" + "_".join([archive, ios_sdk]) + ".ipa"
backupURL = game_id + "/ios/backup/" + "_".join([env("CI_PIPELINE_ID"), archive, ios_sdk, env("RESOURCE_VERSION"), env("VERSION_NAME"), env("GAMECONFIG_URL_TYPE"), env("CI_COMMIT_REF_SLUG"), pipeline_date, sdk_login]) + ".ipa"
downloadURL = "https://repo.bt/repository/" + uploadURL

PlistBuddy("../public/manifest.plist",
    "Set :items:0:assets:0:url " + downloadURL,
    "Set :items:0:metadata:bundle-identifier " + bundle_id,
    "Set :items:0:metadata:bundle-version " + env("VERSION_NAME", "1.0"),
    "Set :items:0:metadata:title " + env("GAME_NAME")
    )

log("上传包体到repo.bt")
ipa_path = grep(find("./ipaout/output", type_filter = "file"), ".ipa")[0]
upload_nexus(ipa_path, backupURL)
upload_nexus(ipa_path, uploadURL)
upload_nexus("../public/manifest.plist", manifestURL)

inform_ipa = "http://repo.bt/repository/" + backupURL
inform_text = "<font color=\"info\">提醒</font>\n" \
    + "流水线：[{}](https://gitlab.bt/{}/pipelines/{}) 已生成ipa包体\n".format(env("CI_PIPELINE_ID"), env("CI_PROJECT_PATH"), env("CI_PIPELINE_ID")) \
    + "下载地址：\n[{}]({})".format(inform_ipa, inform_ipa)
if env("CI_PIPELINE_SOURCE") != "schedule":
    inform_weixinwork(inform_text, env("INFORM_KEY"), msgtype = "markdown")
    inform_weixinwork("", env("INFORM_KEY"), at = env("INFORM_USER", env("GITLAB_USER_LOGIN")))

write_file("../package-url-ios.txt", inform_ipa)
