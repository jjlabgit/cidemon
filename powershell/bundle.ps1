$ErrorActionPreference = "Stop";

if (("true" -eq "$CLEAN_LIBRARY") -and (Test-Path UnityProj/Library)) {
    rm -r -Force UnityProj/Library/*;
    $CLEAN_CACHE="true";
}
if (("true" -eq "$CLEAN_CACHE") -and (Test-Path UnityProj/AssetBundles)) {
    rm -r -Force UnityProj/AssetBundles;
    $BUNDLE_FORCE_REBUILD="true";
}

for ($i=0; $i -lt 3; $i++) {
    $process = Start-Process "${UNITY_EXE_PATH}" -ArgumentList `
        "-batchmode `
        -projectPath ${CI_PROJECT_DIR}/UnityProj `
        -buildTarget ${BUILD_TARGET} `
        -executeMethod ${BUILD_BUNDLE_METHOD} `
        -isForceRebuild ${BUNDLE_FORCE_REBUILD} `
        -platformIndex ${PLATFORM_INDEX} `
        -enableEncodeLua ${BUNDLE_ENABLE_ENCODE_LUA} `
        -language ${BUILD_LANG_CONF} `
        -gameConfigBranchName ${GAMECONFIG_BRANCH_NAME} `
		-branchName ${CI_COMMIT_REF_SLUG} `
        -quit `
        -logFile log.txt" `
        -PassThru;

    Start-Sleep 10;
    $job = Start-Job -Scriptblock {param($p) Get-Content -Path $p/log.txt -Wait | ForEach-Object {$_;}} -ArgumentList (pwd).Path;
    while ($process.HasExited -eq $false) {
        Receive-Job $job;
    }
    Receive-Job $job;
    Stop-Job $job;
    echo "BuildAB return code is :" $process.ExitCode;

    $testFailed = Select-String ./log.txt -Pattern "Failed executing selected SpritePackerPolicy.ArgumentException";
    if ($testFailed) {
        echo "构建出错:Failed executing selected SpritePackerPolicy.ArgumentException";
        exit 2;
    }
    $testFailed = Select-String ./log.txt -Pattern "Receiving unhandled NULL exception";
    if ($testFailed) {
        echo "构建出错:Receiving unhandled NULL exception";
        exit 3;
    }
    $testFailed = Select-String ./log.txt -Pattern "Aborting batchmode due to failure";
    if ($testFailed) {
        echo "构建出错:Aborting batchmode due to failure";
        exit 5;
    }
    $testFailed = Select-String ./log.txt -Pattern "Error: Global Illumination requires a graphics device to render albedo";
    if ($testFailed) {
        echo "构建出错:Error: Global Illumination requires a graphics device to render albedo";
        exit 6;
    }
    $test10086 = Select-String ./log.txt -Pattern "EditorApplicationIsCompiling10086";
    if ($test10086) {
        continue;
    } elseif ($process.ExitCode -ne 0){
        echo "构建出错，详情请查询日志";
        exit $process.ExitCode;
    } else {
        break;
    }
}
    
$test10086 = Select-String ./log.txt -Pattern "EditorApplicationIsCompiling10086";
if ($test10086) {
    echo "构建出错：EditorApplicationIsCompiling10086";
    exit 4;
}
