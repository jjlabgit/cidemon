# coding=utf-8
import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from ci import *

# 删除其他语言的AB资源
lang_dir = "./xcodeprojects/libgame/Data/Raw/aa/IOS/language_assets_language"
build_lang = env("BUILD_LANG", "all")
if os.path.isdir(lang_dir) and build_lang != "all":
    for language in ls(lang_dir):
        if language not in build_lang.split(","):
            rm(os.path.join(lang_dir, language))

log("开始构建IPA包")

archivePath = "./ipaout/output/app.xcarchive"
outputPath = "./ipaout/output"

ios_sdk = env("IOS_SDK")
scheme_dict = {
    "OneSDK": "Unity-iPhone",
    "MSDK": "Unity-iPhone-msdk",
    "UbeejoySDK": "Unity-ubeejoy",
    "UbeejoySDKKR": "Unity-ubeejoy-kr",
    "EfunSDK": "Unity-iPhone-efun",
    "EfunSDKJP": "Unity-iPhone-efun-jp"
}

is_debug = env_bool("IS_DEBUG")
is_distribution = env("IS_DISTRIBUTION")
if is_distribution == "adhoc":
    archiveConfig = "ReleaseForRunning"
    profilePath = "configs/" + ios_sdk + "/profile_appstore_adhoc.plist"
elif is_debug and convert_bool(is_distribution):
    archiveConfig = "DebugForDistribution"
    profilePath = "configs/" + ios_sdk + "/profile_appstore_dev.plist"
elif is_debug and not convert_bool(is_distribution):
    archiveConfig = "Debug"
    profilePath = "./configs/" + ios_sdk + "/profile_enterprise_dev.plist"
elif not is_debug and convert_bool(is_distribution):
    archiveConfig = "ReleaseForDistribution"
    profilePath = "./configs/" + ios_sdk + "/profile_appstore.plist"
elif not is_debug and not convert_bool(is_distribution):
    archiveConfig = "Release"
    profilePath = "./configs/" + ios_sdk + "/profile_enterprise.plist"

bundle_identifier = env("PRODUCT_BUNDLE_IDENTIFIER")
profile_specifier = env("PROVISIONING_PROFILE_SPECIFIER")
plist_method = env("PLIST_METHOD")
if bundle_identifier or profile_specifier:
    if not (bundle_identifier and profile_specifier):
        error("PRODUCT_BUNDLE_IDENTIFIER和PROVISIONING_PROFILE_SPECIFIER需要同时设置！")
    PlistBuddy(profilePath, "Delete :provisioningProfiles:" + bundle_identifier, allow_failure = True)
    PlistBuddy(profilePath, "Add :provisioningProfiles:" + bundle_identifier + " string " + profile_specifier)
if plist_method:
    PlistBuddy(profilePath, "Set :method " + plist_method)

PlistBuddy("xcodeprojects/app/" + ios_sdk + "/Info.plist",
    "Set :CFBundleShortVersionString " + env("VERSION_NAME","1.0"),
    "Set :CFBundleVersion " + env("VERSION_CODE","0")
    )
if os.path.exists("xcodeprojects/app/sticker/Info.plist"):
    PlistBuddy("xcodeprojects/app/sticker/Info.plist",
        "Set :CFBundleShortVersionString " + env("VERSION_NAME","1.0"),
        "Set :CFBundleVersion " + env("VERSION_CODE","0")
        )
if ios_sdk == "MSDK":
    if is_debug:
        PlistBuddy("xcodeprojects/app/MSDK/Info.plist", "Set :MSDK_ENV test" + plist_method)
    else:
        PlistBuddy("xcodeprojects/app/MSDK/Info.plist", "Set :MSDK_ENV release" + plist_method)
else:
    baitian_config = "xcodeprojects/app/" + ios_sdk + "/BaitianConfig.txt"
    sed(baitian_config, find_log(baitian_config, "SDK_Running_type")[0], "SDK_Running_type=" + env("ORG_GRADLE_PROJECT_debugConfig", 2))

log("Archive " + scheme_dict[ios_sdk] + " Version")
shell("xcodebuild archive -workspace xcodeprojects/app/Unity-iPhone.xcworkspace -scheme " + scheme_dict[ios_sdk] + " -configuration " + archiveConfig + " -archivePath " + archivePath)
log("Package " + scheme_dict[ios_sdk] + " Version")
shell("xcodebuild -exportArchive -archivePath " + archivePath + " -exportPath " + outputPath + " -exportOptionsPlist " + profilePath + " -allowProvisioningUpdates")
log("构建IPA包完成")
