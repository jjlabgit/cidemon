#!/bin/sh
set -e

#根据渠道，设置对应要打的包体 路径 是否debug模式 获取其打出包体的路径
if [[ "${ANDROID_SDK}" == "ubeejoy" ]]; then
    BUNDLE_ID=com.ubj.alx.gp
    if [ "assembleGpEnDevDebug" == "$ASSEMBLE_TASK" ]; then
        APK_PATH=./ubeejoy/build/outputs/apk/gpEnDev/debug/ubeejoy-gpEnDev-debug.apk
        DEBUG_PREFIX=opendebug
    else
        APK_PATH=./ubeejoy/build/outputs/apk/gpEnProduct/release/ubeejoy-gpEnProduct-release.apk
        DEBUG_PREFIX=closedebug
    fi
else
    BUNDLE_ID=com.baitian.alx.alxsy.bt
    if [ "true" == "$IS_DEBUG" ]; then
        APK_PATH=./sdkone/build/outputs/apk/debug/sdkone-debug.apk
        DEBUG_PREFIX=opendebug
    else
        APK_PATH=./sdkone/build/outputs/apk/release/sdkone-release.apk
        DEBUG_PREFIX=closedebug
    fi
fi

# 设置当前包体链接的区服
# GAMECONFIG_URL_TYPE 0-内网开发服 1-云测服 2-国服正式服 3-内网测试服 4-国服预发布服
if [ "$GAMECONFIG_URL_TYPE" -eq 0 ]; then
    CONFIG=inner_dev
elif [ "$GAMECONFIG_URL_TYPE" -eq 1 ]; then
    CONFIG=onlinetest
elif [ "$GAMECONFIG_URL_TYPE" -eq 2 ]; then
    CONFIG=online
elif [ "$GAMECONFIG_URL_TYPE" -eq 3 ]; then
    CONFIG=inner_test
elif [ "$GAMECONFIG_URL_TYPE" -eq 4 ]; then
    CONFIG=prerelease
fi

# 取到对应设置路径下的文件名称
MAIN_OBB_NAME=${CI_COMMIT_REF_SLUG}/${CONFIG}/main.versionCode.${BUNDLE_ID}.obb
PATCH_OBB_NAME=${CI_COMMIT_REF_SLUG}/${CONFIG}/patch.versionCode.${BUNDLE_ID}.obb
APK_NAME=${CI_COMMIT_REF_SLUG}_${CONFIG}.apk
DIR_NAME=/home/site/play/${CI_COMMIT_REF_SLUG}/${CONFIG}/

#上传apk 和 obb 文件
scp -r ${APK_PATH} root@10.17.1.108:/home/package_backup/${DEBUG_PREFIX}-${CI_COMMIT_REF_SLUG}-${RESOURCE_VERSION}_${VERSION_NAME}_${GAMECONFIG_URL_TYPE}_${CI_PIPELINE_ID}.apk
scp -r ${APK_PATH} root@10.17.1.107:/home/site/play/${APK_NAME}

if [[ "${SPLIT_OBB}" == "true" ]] && [[ "${FORCE_OLD_OBB}" == "false" ]]; then
    ssh root@10.17.1.107 "[ -d ${DIR_NAME} ] && echo ok || mkdir -p ${DIR_NAME}"
    echo ${MAIN_OBB_NAME}
    scp -r ./${CI_PIPELINE_ID}.obb root@10.17.1.108:/home/package_backup/obb-main-${CI_COMMIT_REF_SLUG}-${RESOURCE_VERSION}_${VERSION_NAME}_${GAMECONFIG_URL_TYPE}_${CI_PIPELINE_ID}.obb
    scp -r ./${CI_PIPELINE_ID}.obb root@10.17.1.107:/home/site/play/${MAIN_OBB_NAME}
	
	if [ -f "./patch_${CI_PIPELINE_ID}.obb" ]; then
		echo ${PATCH_OBB_NAME}
		scp -r ./patch_${CI_PIPELINE_ID}.obb root@10.17.1.108:/home/package_backup/obb-patch-${CI_COMMIT_REF_SLUG}-${RESOURCE_VERSION}_${VERSION_NAME}_${GAMECONFIG_URL_TYPE}_${CI_PIPELINE_ID}.obb
		scp -r ./patch_${CI_PIPELINE_ID}.obb root@10.17.1.107:/home/site/play/${PATCH_OBB_NAME}
	fi
fi
