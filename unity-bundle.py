# coding=utf-8
import string
import time
from ci import *

def copy_library_from_ref(ref):
    if ref == env("CI_COMMIT_REF_NAME"):
        error("不能复制分支自己的缓存！")
    ref_slug = re.sub(r'['+ re.escape(string.punctuation) + ']', '-', ref)
    library_path = ci_home("cache-lib", env("CI_PROJECT_PATH"), env("PLATFORM") + "-" + ref_slug, "UnityProj/Library")
    if not os.path.exists(library_path):
        error("分支{}的Library缓存不存在！".format(ref))

    # 保证复制缓存的时候源分支没有在跑流水线
    wait_flag = True
    while wait_flag:
        wait_flag = False
        running = gitlab_api("projects/{}/pipelines?status=running".format(env("CI_PROJECT_ID")))
        for pipeline in running:
            if pipeline["ref"] == ref:
                variables = gitlab_api("projects/{}/pipelines/{}/variables".format(env("CI_PROJECT_ID"), pipeline["id"]))
                if not variables:
                    wait_flag = True
                    break
                else:
                    remote_machine = env("SELECT_MACHINE")
                    remote_platform = env("PLATFORM")
                    for var in variables:
                        if var["key"] == "SELECT_MACHINE":
                            remote_machine = var["value"]
                        if var["key"] == "PIPELINE_PLATFORM":
                            remote_platform = var["value"]
                    if remote_machine == env("SELECT_MACHINE") and env("PLATFORM") in remote_platform:
                        wait_flag = True
                        break
        if wait_flag:
            log("流水线{}正在运行，需等待其执行完后才能复制分支{}的缓存".format(pipeline["id"], pipeline["ref"]))
            time.sleep(60)
    rm("UnityProj/Library/*")
    log("正在复制分支{}的缓存......".format(ref))
    cp(library_path + "/*", "UnityProj/Library")
    log("复制分支{}的缓存完成！".format(ref))


if __name__ == '__main__':
    isForceRebuild = env_bool("CLEAN_CACHE")
    if env_bool("CLEAN_LIBRARY"):
        log("清空Library目录：UnityProj/Library/*")
        rm("UnityProj/Library/*")
        isForceRebuild = True
    elif env("COPY_LIBRARY_FROM_REF"):
        copy_library_from_ref(env("COPY_LIBRARY_FROM_REF"))

    if isForceRebuild:
        log("强打删除目录：UnityProj/Library/com.unity.addressables/")
        rm("UnityProj/Library/com.unity.addressables/")

    buildTarget, platformIndex = getUnityPlatformInfo(env("PLATFORM"))
    # 先打开一次Unity导入preset文件，再重置被其他preset错误配置的png文件，打AB的时候会重新按照新的preset导入
    unity(buildTarget = buildTarget, logFile = "import.txt")
    shell("git reset --hard")

    if env_bool("ENABLE_SAMPLE_SHADER_VARIANT"):
        cp("customshadervariants_format.yaml", "UnityProj/Assets/GameAssets/shared/urpshaders/customshadervariants_format.yaml")
        cp("customshadervariants.shadervariants", "UnityProj/Assets/GameAssets/shared/urpshaders/customshadervariants.shadervariants")

    enableEncodeLua = True
    # isDebug, isProfiler, versionName, versionCode, gameConfigUrlType, gameConfigBranchName, gameConfigLanguageType, gameConfigSupportLanguageType
    return_code = unity(
        buildTarget = buildTarget,
        executeMethod = "Astral.Core.AutoPackage.AutoBuildAssetBundle",
        platformIndex = platformIndex,
        isForceRebuild = isForceRebuild,
        # isForceRebuild = "false", # 为true的时候似乎一定会改变所有AB文件的MD5，导致热更失效
        enableEncodeLua = enableEncodeLua,
        language = env("BUILD_LANG_DEFAULT"),
        gameConfigBranchName = env("CI_COMMIT_REF_NAME")
    )

    error_logs = [
        # 此处不能输入中文
        "Failed executing selected SpritePackerPolicy.ArgumentException",
        "Receiving unhandled NULL exception",
        "EditorApplicationIsCompiling10086",
        "Failed to compile player scripts",
        "Build Task WriteSerializedFiles failed with exception",
        "error! Subprogram failed",
        "=>Object reference not set to an instance of an object",
        "ArgumentOutOfRangeException: Index was out of range."
    ]
    for error_log in error_logs:
        if find_log("import.txt", error_log) or find_log("log.txt", error_log):
            error("构建出错: " + error_log)

    exit(return_code)
