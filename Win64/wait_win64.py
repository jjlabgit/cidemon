# coding=utf-8

import os
import sys
import time
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from ci import *

if __name__ == "__main__":
    if not env("ACCESS_TOKEN"):
        error("ACCESS_TOKEN没有设置，请前往Secret Variables选项设置")
    
    max_wait_min = 60
    query_interval_sec = 30

    for i in range(int(60 / query_interval_sec * max_wait_min)):
        pending = gitlab_api("projects/{}/pipelines?scope=pending".format(env("CI_PROJECT_ID")))
        running = gitlab_api("projects/{}/pipelines?scope=running".format(env("CI_PROJECT_ID")))

        wait_flag = False
        for pipeline in pending + running:
            pipeline_id = int(pipeline["id"])
            if pipeline_id < int(env("CI_PIPELINE_ID")):
                branch = pipeline["ref"]
                jobs = gitlab_api("projects/{}/pipelines/{}/jobs".format(env("CI_PROJECT_ID"), pipeline_id))
                is_win64 = False
                for job in jobs:
                    if job["name"] == "win64:wait" or job["name"] == "win64:bundle":
                        is_win64 = True
                        break
                if branch == env("CI_COMMIT_REF_NAME") and is_win64:
                    wait_flag = True
                    log("未完成流水线" + str(pipeline_id) + "等待中......")
                    break

        if not wait_flag:
            exit()
        else:
            time.sleep(query_interval_sec)
    error("超过最大等待时间，正在退出...")
