# coding=utf-8
# pylint: disable=print-statement
from pbxproj import XcodeProject
from pbxproj.pbxextensions import TreeType
from pbxproj.pbxextensions.ProjectFiles import FileOptions
from codecs import open
import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from ci import *
reload(sys)
sys.setdefaultencoding('utf8')

def read_excluded_targets_from_file(filename):
    result = []
    if (not filename) or (not len(filename)) or (not os.path.isfile(filename)):
        return result
    with open(filename, "r", encoding="utf-8") as f:
        for line in f.readlines():
            line = line.strip()  
            if len(line) > 0:
                result.append(line)
    return result

if __name__ == "__main__":
    log("修改UNITY_USES_REMOTE_NOTIFICATIONS定义值")
    sed("./xcodeprojects/libgame/Classes/Preprocessor.h","#define UNITY_USES_REMOTE_NOTIFICATIONS 0","#define UNITY_USES_REMOTE_NOTIFICATIONS 1")

    cd("xcodeprojects/app")

    # IS_DEBUG和IS_DISTRIBUTION在CI配置和trigger定义赋值。
    IS_DEBUG = os.getenv("IS_DEBUG")
    IS_DISTRIBUTION = os.getenv("IS_DISTRIBUTION")
    if IS_DEBUG and IS_DISTRIBUTION:
        buildOpt = IS_DEBUG + IS_DISTRIBUTION
    else:
        buildOpt = "truefalse"

    if buildOpt == "truetrue":
        archiveConfig="DebugForDistribution"
    elif buildOpt == "truefalse":
        archiveConfig="Debug"
    elif buildOpt == "falsetrue":
        archiveConfig="ReleaseForDistribution"
    elif buildOpt == "falsefalse":
        archiveConfig="Release"
    elif (buildOpt == "trueadhoc") or (buildOpt == "falseadhoc"):
        archiveConfig="ReleaseForRunning"
    else:
        archiveConfig="Debug"

    configTag = ["CODE_SIGN_IDENTITY", "DEVELOPMENT_TEAM", "PRODUCT_BUNDLE_IDENTIFIER", "PROVISIONING_PROFILE", "PROVISIONING_PROFILE_SPECIFIER"]
    configMap = {}

    for tag in configTag:
        if os.getenv(tag) != None and os.getenv(tag) != "":
            configMap[tag] = os.getenv(tag)

    project_file_path = "./Unity-iPhone.xcodeproj/project.pbxproj"
    project = XcodeProject.load(project_file_path)
    # Classes
    classes_path = os.path.join(os.path.dirname(project_file_path), "../../libgame/Classes")
    print "Add \"Classes/*\" to Xcode project."
    project.add_folder(classes_path, excludes=["^DynamicLibEngineAPI.mm$", "^DynamicLibEngineAPI-functions.h$", "^Vector3.h$", "^\.DS_Store$"], file_options=FileOptions(ignore_unknown_type=True), target_name='UnityFramework')
    # http://wiki.info/pages/viewpage.action?pageId=61168347
    classes_main_parent = project.get_or_create_group(os.path.split(classes_path)[1], classes_path, None)
    classes_main_path = os.path.join(os.path.dirname(project_file_path), "../../libgame/Classes/main.mm")
    project.add_file(classes_main_path, parent = classes_main_parent, target_name = 'UnityFramework', tree = "SOURCE_ROOT", force = True)

    # Libraries
    lib_path = os.path.join(os.path.dirname(project_file_path), "../../libgame/Libraries")
    print "Add \"Libraries/*\" to Xcode project."
    project.add_folder(lib_path, excludes=["^external$", "^libil2cpp$", "^IL2CPPOptions.cpp.cpp$", "^com\.unity\.ads$", "^\.DS_Store$"], file_options=FileOptions(ignore_unknown_type=True), target_name='UnityFramework')

    excluded_targets_sources_build = read_excluded_targets_from_file("./targets_excluded_unity_code.txt")
    excluded_targets_set_flags = read_excluded_targets_from_file("./targets_excluded_set_flags.txt")
    targets = project.objects.get_targets()
    for target in targets:
        if target.name in excluded_targets_sources_build:
            print "Excluded target in sources build: " + target.name
            for build_phase_id in target.buildPhases:
                target_build_phase = target.get_parent()[build_phase_id]
                current_build_phase = target_build_phase.isa
                if current_build_phase == u"PBXSourcesBuildPhase":
                    target.remove_build_phase(target_build_phase)
        if target.name not in excluded_targets_set_flags:
            print "Set flags in target: " + target.name
            for tag in configMap:
                project.set_flags(tag, configMap[tag], target_name=target.name, configuration_name=archiveConfig)

    project.save()

    log("CocoaPods同步库文件依赖")

    shell("pod deintegrate")
    shell("pod cache clean --all")
    shell("pod repo update")
    shell("pod install")
    shell("pod update")
