# coding=utf-8
from ci import *
import xml.etree.ElementTree as ET
from hotupdate import indent
import sys
import json

def write_manifest(filename, env_name, key = 'version'):
    if env(env_name):
        data = {}
        data[key] = env(env_name)
        json.dump(data, open(filename, 'w'))

def write_version_info(filename, title, env_name):
    if env(env_name):
        write_file(filename, title + ": " + env(env_name))

def get_attr(namespace, name = "name"):
    return "{"+ namespace + "}" + name

def add_permission_if_not_exist(permission, root, namespace):
    name = get_attr(namespace)
    for child in root.iter('uses-permission'):
        if (child.get(name) == permission):
            return
    perm = ET.Element('uses-permission')
    perm.set(name, permission)
    root.insert(-1, perm)

def modifyAndroidManifest():
    manifest = 'AndroidProj/{}/src/main/AndroidManifest.xml'.format(env("ANDROID_SDK"))
    tree = ET.parse(manifest)
    root = tree.getroot()
    android = "http://schemas.android.com/apk/res/android"
    ET.register_namespace('android', android)
    ET.register_namespace('tools', "http://schemas.android.com/tools")
    # 添加uses-permission权限
    gpu_permissions = [
        'android.permission.INTERNET',
        'com.qti.permission.PROFILER'
    ]
    for permission in gpu_permissions:
        add_permission_if_not_exist(permission, root, android)
    # application添加debuggable属性
    sub = root.find('application')
    sub.set(get_attr(android, "debuggable"), 'true')
    # 写入文件
    indent(root)
    tree.write(manifest, encoding="utf-8", xml_declaration=True)

if __name__ == '__main__':
    if env_bool("GPU_DEBUG"):
        modifyAndroidManifest()

    mkdir("UnityProj/Assets/StreamingAssets")
    info_file = "UnityProj/Assets/StreamingAssets/version_info.txt"
    write_version_info(info_file, "Pipeline ID", "CI_PIPELINE_ID")
    write_version_info(info_file, "Version Name", "VERSION_NAME")
    write_version_info(info_file, "Version Code", "VERSION_CODE")
    write_version_info(info_file, "Resource Version", "RESOURCE_VERSION")
    write_version_info(info_file, "Dolphin Program Version", "DOLPHIN_PROGRAM_VERSION")
    write_version_info(info_file, "Dolphin Source Version", "DOLPHIN_SOURCE_VERSION")

    mkdir("UnityProj/Assets/Resources")
    write_manifest("UnityProj/Assets/Resources/version.manifest", "RESOURCE_VERSION")
    write_manifest("UnityProj/Assets/Resources/dolphin_program_version.manifest", "DOLPHIN_PROGRAM_VERSION")
    write_manifest("UnityProj/Assets/Resources/dolphin_source_version.manifest", "DOLPHIN_SOURCE_VERSION")
